﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionAchatErp.Pages
{
    public static class MainMenu
    {
       
        public static class User
        {
            public const string PageName = "Utilisateur";
            public const string RoleName = "Utilisateur";
            public const string Path = "/UserRole/Index";
            public const string ControllerName = "UserRole";
            public const string ActionName = "Index";
        }

        public static class ChangePassword
        {
            public const string PageName = "Changer Mot de passe";
            public const string RoleName = "Changer Mot de passe";
            public const string Path = "/UserRole/ChangePassword";
            public const string ControllerName = "UserRole";
            public const string ActionName = "ChangePassword";
        }

        public static class Role
        {
            public const string PageName = "Rôle";
            public const string RoleName = "Rôle";
            public const string Path = "/UserRole/Role";
            public const string ControllerName = "UserRole";
            public const string ActionName = "Role";
        }

        public static class ChangeRole
        {
            public const string PageName = "Changer de rôle";
            public const string RoleName = "Changer de rôle";
            public const string Path = "/UserRole/ChangeRole";
            public const string ControllerName = "UserRole";
            public const string ActionName = "ChangeRole";
        }

        public static class Dashboard
        {
            public const string PageName = "Tableau de bord";
            public const string RoleName = "Tableau de bord";
            public const string Path = "/Dashboard/Index";
            public const string ControllerName = "Dashboard";
            public const string ActionName = "Index";
        }


        //DEMANDE ACHATS
        public static class DemandeAchatNatures
        {
            public const string PageName = "Nature Demande Achat";
            public const string RoleName = "Nature Demande Achat";
            public const string Path = "/DemandeAchatNatures/Index";
            public const string ControllerName = "DemandeAchatNatures";
            public const string ActionName = "Index";
        }
        public static class ExpressionBesoins
        {
            public const string PageName = "Expression Besoins";
            public const string RoleName = "Expression Besoins";
            public const string Path = "/ExpressionBesoins/Index";
            public const string ControllerName = "ExpressionBesoins";
            public const string ActionName = "Index";
        }
        public static class SuivreExpressionBesoin
        {
            public const string PageName = "Suivre Expression Besoins";
            public const string RoleName = "Expression Besoins";
            public const string Path = "/ExpressionBesoins/SuivreMesBesoins";
            public const string ControllerName = "ExpressionBesoins";
            public const string ActionName = "Suivre";
        }
        public static class ExpressionBesoinsAvalider
        {
            public const string PageName = "Expression Besoins à valider";
            public const string RoleName = "Expression Besoins à valider";
            public const string Path = "/ExpressionBesoins/ListeValider";
            public const string ControllerName = "ExpressionBesoins";
            public const string ActionName = "Index";
        }

        public static class ExpressionBesoinsAvaliderCRB
        {
            public const string PageName = "Validation CRB - Expression Besoins";
            public const string RoleName = "Expression Besoins à valider CRB";
            public const string Path = "/ExpressionBesoins/ListeValiderCrb";
            public const string ControllerName = "ExpressionBesoins";
            public const string ActionName = "Index";
        }
        public static class ExpressionBesoinsAvaliderRespCRB
        {
            public const string PageName = "Validation CRB - Expression Besoins";
            public const string RoleName = "Expression Besoins à valider Resp. CRB";
            public const string Path = "/ExpressionBesoins/ListeValiderRCrb";
            public const string ControllerName = "ExpressionBesoins";
            public const string ActionName = "Index";
        }
        public static  class BesoinsEnAttente
        {
            public const string PageName = "Expression Besoins En Attente";
            public const string RoleName = "Demande Achat";
            public const string Path = "/DemandeAchats/BesoinsEnAttente";
            public const string ControllerName = "DemandeAchats";
            public const string ActionName = "BesoinsEnAttente";
        }
        public static class DemandeAchats
        {
            public const string PageName = "Liste Demandes Achats";
            public const string RoleName = "Demande Achat";
            public const string Path = "/DemandeAchats/Index";
            public const string ControllerName = "DemandeAchats";
            public const string ActionName = "Index";
        }

        public static class ValidationRespRafe
        {
            public const string PageName = "Liste Demandes Achats A Valider";
            public const string RoleName = "Demande Achat";
            public const string Path = "/DemandeAchats/ValidationsRafe";
            public const string ControllerName = "DemandeAchats";
            public const string ActionName = "ValidationsRafe";
        }
        public static class Directions
        {
            public const string PageName = "Direction";
            public const string RoleName = "Direction";
            public const string Path = "/Directions/Index";
            public const string ControllerName = "Directions";
            public const string ActionName = "Index";
        }
        public static class Departements
        {
            public const string PageName = "Departements";
            public const string RoleName = "Departements";
            public const string Path = "/Departements/Index";
            public const string ControllerName = "Departements";
            public const string ActionName = "Index";
        }

        public static class CategorieItems
        {
            public const string PageName = "Categorie Produit";
            public const string RoleName = "CategorieItems";
            public const string Path = "/CategorieItems/Index";
            public const string ControllerName = "CategorieItems";
            public const string ActionName = "Index";
        }

        public static class Items
        {
            public const string PageName = "Produits";
            public const string RoleName = "Items";
            public const string Path = "/Items/Index";
            public const string ControllerName = "Items";
            public const string ActionName = "Index";
        }

        public static class Signatures
        {
            public const string PageName = "Signature";
            public const string RoleName = "Signature";
            public const string Path = "/Signatures/Index";
            public const string ControllerName = "Signatures";
            public const string ActionName = "Index";
        }

        public static class Statuts
        {
            public const string PageName = "Statuts";
            public const string RoleName = "Statuts";
            public const string Path = "/Statuts/Index";
            public const string ControllerName = "Statuts";
            public const string ActionName = "Index";
        }

        public static class CentreBudgetaire
        {
            public const string PageName = "Centre Budgetaire";
            public const string RoleName = "CentreBudgetaires";
            public const string Path = "/CentreBudgetaires/Index";
            public const string ControllerName = "CentreBudgetaires";
            public const string ActionName = "Index";
        }

        public static class TypeMarche
        {
            public const string PageName = "Type Marche";
            public const string RoleName = "TypeMarche";
            public const string Path = "/TypeMarches/Index";
            public const string ControllerName = "TypeMarches";
            public const string ActionName = "Index";
        }



    }
}
