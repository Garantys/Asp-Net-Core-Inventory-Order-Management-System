using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();
/*************OCO******************/
//a variable to hold configuration
IConfiguration Configuration;
Configuration = builder.Configuration;
// call some methods
//ConfigureAuth(builder.Services);
ConfigureRedis(builder.Services);
ConfigureSession(builder.Services);
ConfigureMvc(builder.Services);
ConfigureServices(builder.Services);
/***********OCO*******************/

var app = builder.Build();
ConfigureDatas(app);
ConfigureMiddleWare(app);

 app.Run();


/******************METHODS********************/
void ConfigureMvc(IServiceCollection services)
{
    builder.Services.AddMvc(options =>
    {
        //var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
        //options.Filters.Add(new AuthorizeFilter(policy));
        options.Filters.Add(new AuthorizeFilter());
    })
    //.AddRazorPagesOptions(options => { options.Conventions.AddPageRoute("/Home/Login", ""); })
    .AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
        options.JsonSerializerOptions.PropertyNamingPolicy = null;
    });
}
void ConfigureServices(IServiceCollection services)
{
    builder.Services.AddDbContext<ApplicationDbContext>(options =>
        options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

    // Get Identity Default Options
    IConfigurationSection identityDefaultOptionsConfigurationSection = Configuration.GetSection("IdentityDefaultOptions");

    services.Configure<IdentityDefaultOptions>(identityDefaultOptionsConfigurationSection);

    var identityDefaultOptions = identityDefaultOptionsConfigurationSection.Get<IdentityDefaultOptions>();

    services.AddIdentity<ApplicationUser, IdentityRole>(options =>
    {
        // Password settings
        options.Password.RequireDigit = identityDefaultOptions.PasswordRequireDigit;
        options.Password.RequiredLength = identityDefaultOptions.PasswordRequiredLength;
        options.Password.RequireNonAlphanumeric = identityDefaultOptions.PasswordRequireNonAlphanumeric;
        options.Password.RequireUppercase = identityDefaultOptions.PasswordRequireUppercase;
        options.Password.RequireLowercase = identityDefaultOptions.PasswordRequireLowercase;
        options.Password.RequiredUniqueChars = identityDefaultOptions.PasswordRequiredUniqueChars;

        // Lockout settings
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(identityDefaultOptions.LockoutDefaultLockoutTimeSpanInMinutes);
        options.Lockout.MaxFailedAccessAttempts = identityDefaultOptions.LockoutMaxFailedAccessAttempts;
        options.Lockout.AllowedForNewUsers = identityDefaultOptions.LockoutAllowedForNewUsers;

        // User settings
        options.User.RequireUniqueEmail = identityDefaultOptions.UserRequireUniqueEmail;

        // email confirmation require
        options.SignIn.RequireConfirmedEmail = identityDefaultOptions.SignInRequireConfirmedEmail;
    })
        .AddEntityFrameworkStores<ApplicationDbContext>()
        .AddDefaultTokenProviders();

    // cookie settings
    services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        options.Cookie.HttpOnly = identityDefaultOptions.CookieHttpOnly;
        options.Cookie.Expiration = TimeSpan.FromDays(identityDefaultOptions.CookieExpiration);
        options.LoginPath = identityDefaultOptions.LoginPath; // If the LoginPath is not set here, ASP.NET Core will default to /Account/Login
        options.LogoutPath = identityDefaultOptions.LogoutPath; // If the LogoutPath is not set here, ASP.NET Core will default to /Account/Logout
        options.AccessDeniedPath = identityDefaultOptions.AccessDeniedPath; // If the AccessDeniedPath is not set here, ASP.NET Core will default to /Account/AccessDenied
        options.SlidingExpiration = identityDefaultOptions.SlidingExpiration;
        options.ExpireTimeSpan = TimeSpan.FromMinutes(120);
    });

    // Get SendGrid configuration options
    services.Configure<SendGridOptions>(Configuration.GetSection("SendGridOptions"));

    // Get SMTP configuration options
    services.Configure<SmtpOptions>(Configuration.GetSection("SmtpOptions"));

    // Get Super Admin Default options
    services.Configure<SuperAdminDefaultOptions>(Configuration.GetSection("SuperAdminDefaultOptions"));

    // Add email services.
    services.AddTransient<IEmailSender, EmailSender>();

    services.AddTransient<INumberSequence, GestionAchatErp.Services.NumberSequence>();

    services.AddTransient<IRoles, Roles>();

    services.AddTransient<IFunctional, Functional>();
}


void ConfigureSession(IServiceCollection services)
{
    IConfigurationSection identityDefaultOptionsConfigurationSection = Configuration.GetSection("IdentityDefaultOptions");

    var identityDefaultOptions = identityDefaultOptionsConfigurationSection.Get<IdentityDefaultOptions>();

    builder.Services.AddSession(options =>
    {
        options.Cookie.Name = "mygreatsite_session";
        options.IdleTimeout = TimeSpan.FromMinutes(60);
    });
 
}
void ConfigureRedis(IServiceCollection services)
{
    /*
    var redisConfig = new RedisOptions();
    Configuration.GetSection(RedisOptions.RedisConfig).Bind(redisConfig);
    services.AddStackExchangeRedisCache(options =>
    {
        options.Configuration = redisConfig.ConnectionString;
        options.InstanceName = "mygreatsite_";
    });
    services.AddDataProtection()
        .SetApplicationName("MyGreatSite.Website")
        .PersistKeysToStackExchangeRedis(ConnectionMultiplexer.Connect(redisConfig.ConnectionString), "DataProtection-Keys");
    */
}

void ConfigureMiddleWare(WebApplication app)
{
    if (builder.Environment.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
        //app.UseBrowserLink();
    }
    else
    {
        app.UseExceptionHandler("/Home/Error");
    }

    app.UseStaticFiles();
    app.UseRouting();
    app.UseCors("default");
    app.UseCookiePolicy();
    app.UseAuthentication();
    app.UseAuthorization();
    app.UseSession();

    app.UseEndpoints(endpoints =>
    {
        endpoints.MapDefaultControllerRoute().RequireAuthorization();
        endpoints.MapControllerRoute(
            name: "Default",
            pattern: "{controller=Home}/{action=Index}"
        );
    });
}

void ConfigureDatas(WebApplication app)
{
    using (var scope = app.Services.CreateScope())
    {
        var services = scope.ServiceProvider;
        try
        {
            var context = services.GetRequiredService<ApplicationDbContext>();
            var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = services.GetRequiredService<RoleManager<IdentityRole>>();
            var functional = services.GetRequiredService<IFunctional>();

            DbInitializer.Initialize(context, functional).Wait();
        }
        catch (Exception ex)
        {
            var logger = services.GetRequiredService<ILogger<Program>>();
            logger.LogError(ex, "An error occurred while seeding the database.");
        }
    }

}
