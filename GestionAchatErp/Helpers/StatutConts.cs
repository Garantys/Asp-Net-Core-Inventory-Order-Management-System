﻿namespace GestionAchatErp.Helpers
{
    public class StatutConts
    {
        public const string CREER = "CREER";
        public const string SOUMETTRE = "SOUMETTRE";
        public const string VALIDRDEMANDEUR = "ACCEPTER";
        // STATUT VALIDATION CRB
        public const string VALIDCRB = "VALIDCRB";
        public const string VALIDRESPCRB = "VALIDRESPCRB";
        public const string TRANSMDA= "TRANSFORMER EN DA";

        // DEMANDE D'ACHAT
        public const string CREERPFRAFE = "CREER";
        public const string SOUMETTREPFRAFE = "SOUMETTRE";
        public const string VALIDRAFE = "VALIDRAFE";
        public const string BUDGETISER = "BUDGETISER";
        public const string VALIDER = "VALIDER";
        public const string BCIMPRIME = "BCIMPRIME";
        public const string REJETER = "REJETER";
        public const string TERMINER = "TERMINER";

    }

    public class ElementConts
    {
        public const string ExpressionBesoin = "ExpressionBesoin";
        public const string DemandeAchat = "DemandeAchat";
    }
}
