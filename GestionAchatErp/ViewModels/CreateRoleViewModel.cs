﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace GestionAchatErp.ViewModels
{
    public class CreateRoleViewModel
    {
        public string RoleName { get; set; }
    }
}
