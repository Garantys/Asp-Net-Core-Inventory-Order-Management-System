﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GestionAchatErp.Models
{
    public class UserProfile
    {
        public int UserProfileId { get; set; }
        public int DirectionId { get; set; }
        public int DepartementId { get; set; }
        public int FonctiontId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string LibelleFonction { get; set; }
        public string LibelleDirection { get; set; }
        public string LibelleDepartement { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string OldPassword { get; set; }
        public string ProfilePicture { get; set; } = "/upload/blank-person.png";

        public string ApplicationUserId { get; set; }
        public virtual Departement Departement { get; set; }
    }
}
