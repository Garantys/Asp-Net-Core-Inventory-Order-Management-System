﻿namespace GestionAchatErp.Models
{
    public class ItemsDemandeAchat
    {
        public int Id { get; set; }
        public int DemandeAchatId { get; set; }
        public int CategorieItemId { get; set; }
        public string CategorieItemLibelle { get; set; }
        public int ItemId { get; set; }
        public string Designation { get; set; }
        public string Reference { get; set; }
        public int Quantite { get; set; }
        public int PrixUnitaire { get; set; }
        public int MontantTotal { get; set; }

        public virtual DemandeAchat DemandeAchat { get; set; }

    }
}
