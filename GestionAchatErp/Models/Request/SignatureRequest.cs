﻿namespace GestionAchatErp.Models.Request
{
    public class SignatureRequest
    {
        public IFormFile SignaturePath { get; set; }
        public string AuteurSignature { get; set; }
    }
}
