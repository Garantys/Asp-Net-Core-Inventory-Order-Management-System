﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GestionAchatErp.Models
{
    public class ExpressionBesoin
    {
        public int Id { get; set; }
        public int UserProfileId { get; set; }
        public int UserIdResponsable { get; set; }
        public int DirectionId { get; set; }
        public int DepartementId { get; set; }
        public int FonctionId { get; set; }
        public int SourceBudgetaire { get; set; }
        public int CrbEnCharge { get; set; }

        [Display(Name = "Nom du demandeur")]
        public string NomDemandeur { get; set; }
        [Display(Name = "Direction")]
        public string DirectionLibelle { get; set; }
        [Display(Name = "Département")]
        public string DepartementLibelle { get; set; }
        public string Fonction { get; set; }
        [Display(Name = "Numéro du besoin")]
        public string NumExpressionBesoin { get; set; }
        [Display(Name = "Nature du besoin")]
        public string NatureBesoin { get; set; }
        [Display(Name = "Motif de la demande")]
        public string MotifDemande { get; set; }
        [Display(Name = "Date de la demande")]
        public DateTime DateDemande { get; set; }
        [Display(Name = "Statut de la demande")]
        public string DernierStatut { get; set; }
        [Display(Name = "Date Statut")]
        public DateTime DateStatut { get; set; }
        public string Observation { get; set; }
        public string Commentaire { get; set; }
        public virtual UserProfile UserProfile { get; set; }


    }
}
