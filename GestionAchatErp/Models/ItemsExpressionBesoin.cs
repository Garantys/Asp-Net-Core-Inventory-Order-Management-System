﻿namespace GestionAchatErp.Models
{
    public class ItemsExpressionBesoin
    {
        public int Id { get; set; }
        public int ExpressionBesoinId { get; set; }
        public int CategorieItemId { get; set; }
        public string CategorieItemLibelle { get; set; }
        public int ItemId { get; set; }
        public string Designation { get; set; }
        public string Reference { get; set; }
        public int Quantite { get; set; }
        public virtual ExpressionBesoin ExpressionBesoin { get; set; }

        
    }
}
