﻿using Microsoft.Extensions.Hosting;
using System.Reflection.Metadata;

namespace GestionAchatErp.Models
{
    public class DemandeAchat
    {
        public int DemandeAchatId { get; set; }
        public int UserId { get; set; }
        public int UserResponsableId { get; set; }
        public int UserResponsableCrbId { get; set; }
        public int DirectionId { get; set; }
        public int DepartementId { get; set; }
        public int FonctionId { get; set; }
        public string DemandeurId { get; set; }
        public string DemandeurNom { get; set; }
        public int ExpressionBesoinId { get; set; }
        public string Direction { get; set; }
        public string Departement { get; set; }
        public string Fonction { get; set; }
        public string NumDemande { get; set; }
        public int CrbEnCharge { get; set; }
        public string TypeAchat { get; set; }
        public string TypeMarche { get; set; }
        public string DemandeMac { get; set; }
        public string NatureBesoin { get; set; }
        public string MotifDemande { get; set; }
        public string ImputationBudgetaire { get; set; }
        public string CreditAlloue { get; set; }
        public string ModificationBudgetaire { get; set; }
        public string EngagementAnterieur { get; set; }
        public string CreditDisponible { get; set; }
        public DateTime? DateDemande { get; set; }
        public string DernierStatut { get; set; }
        public DateTime? DateStatut { get; set; }
        public string Observation { get; set; }
        public string Commentaire { get; set; }
        public int DemandeAchatNatureId { get; set; }
    }
}
