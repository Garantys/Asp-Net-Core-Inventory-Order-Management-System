﻿using Newtonsoft.Json;

namespace GestionAchatErp.Models
{
    public class Departement
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }
        public int DirectionId { get; set; }
        public string DirectionLibelle { get; set; }
        [JsonIgnore]
        public virtual Direction Direction { get; set; }
        public virtual List<UserProfile> UserProfile { get; set; }



    }
}
