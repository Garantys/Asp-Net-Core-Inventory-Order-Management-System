﻿namespace GestionAchatErp.Models
{
    public class HistoriqueStatut
    {
        public int Id { get; set; }
        public string Element { get; set; }
        public int ElementId { get; set; }
        public int UserProfileId { get; set; }
        public string Statut { get; set; }
        public DateTime DateDebutStatut { get; set;}
        public DateTime DateFinStatut { get; set; }
        public DateTime DateCreation { get; set; }
        public string Commentaire { get; set; }
        public virtual UserProfile UserProfile { get; set; }

    }
}
