﻿namespace GestionAchatErp.Models
{
    public class CategorieItems
    {
        public int Id { get; set; }
        public string Designation { get; set; }
    }
}
