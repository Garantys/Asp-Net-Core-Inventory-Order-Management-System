﻿namespace GestionAchatErp.Models
{
    public class DemandeAchatItem
    {
        public int DemandeAchatItemId { get; set; }
        public string Reference { get; set; }
        public int CategorieItemId { get; set; }
        public string CategorieItemLibelle { get; set; }
        public int ItemId { get; set; }
        public string Designation { get; set; }
        public int quantite { get; set; }


        public int DemandeAchatId { get; set; }
        public virtual DemandeAchat DemandeAchat { get; set; }
    }
}
