﻿namespace GestionAchatErp.Models
{
    public class DocumentAchat
    {
        public int Id { get; set; }
        public string TypeDocument { get; set; }
        public string ElementType { get; set; }
        public string DocumentUrl { get; set;}

    }
}
