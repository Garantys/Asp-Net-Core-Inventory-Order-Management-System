﻿namespace GestionAchatErp.Models
{
    public class Items
    {
        public int Id { get; set; }
        public string Designation { get; set; }
        public string Reference { get; set; }
        public int CategorieItemsId { get; set; }
        public virtual CategorieItems CategorieItems { get; set; }
    }
}
