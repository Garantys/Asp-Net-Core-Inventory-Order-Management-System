﻿namespace GestionAchatErp.Models
{
    public class DemandeAchatStatutHistory
    {
        public  int DemandeAchatStatutHistoryId { get; set; }
        public string StatutName { get; set; }
        public string OldStatutName { get; set; }
        public DateTime DateCreatedDebut { get; set; } 
        public DateTime? DateCreatedFin { get; set; } 
        public string Commentaire { get; set; }


        public int DemandeAchatId { get; set; }
        public virtual DemandeAchat DemandeAchat { get; set; }
    }
}
