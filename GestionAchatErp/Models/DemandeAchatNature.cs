﻿namespace GestionAchatErp.Models
{
    public class DemandeAchatNature
    {
        public int DemandeAchatNatureId { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }
        public int duree { get; set; }

    }
}
