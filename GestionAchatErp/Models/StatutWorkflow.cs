﻿namespace GestionAchatErp.Models
{
    public class StatutWorkflow
    {
        public int StatutWorkflowId { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }
        public string Niveau { get; set; }
        public int Poids { get; set; }
    }
}
