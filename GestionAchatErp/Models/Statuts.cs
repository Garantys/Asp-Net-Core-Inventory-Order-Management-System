﻿namespace GestionAchatErp.Models
{
    public class Statuts
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }
        public string Etape { get; set; }
        public int Poids { get; set; }
        //public string etat { get; set; }
    }
}
