﻿using System.ComponentModel;

namespace GestionAchatErp.Models
{
    public class Fonction
    {
        public int Id { get; set; }
        public string Code { get; set; }
        [DisplayName("Libellé")]
        public string Libelle { get; set; }
        public string  CreateBy { get; set; }
        public string UpdateBy { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
    }
}
