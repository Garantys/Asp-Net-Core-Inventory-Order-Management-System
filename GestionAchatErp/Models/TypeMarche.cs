﻿namespace GestionAchatErp.Models
{
    public class TypeMarche
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Libelle { get; set; }
    }
}
