﻿namespace GestionAchatErp.Models
{
    public class Signature
    {
        public int Id { get; set; }
        public int UserProfileId { get; set; }
        public int DirectionId { get; set; }
        public int DepartementId { get; set; }
        public string SignaturePath { get; set;}
        public string AuteurSignature { get; set; }
        public string DirectionLibelle { get; set; }
        public string DepartementLibelle { get; set; }

        public DateTimeOffset DateSignature { get; set; }
        public virtual UserProfile UserProfile { get; set; }

    }
}
