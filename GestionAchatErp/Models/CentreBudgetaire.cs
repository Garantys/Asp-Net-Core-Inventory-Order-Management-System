﻿namespace GestionAchatErp.Models
{
    public class CentreBudgetaire
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string LibelleCb { get; set; }
    }
}
