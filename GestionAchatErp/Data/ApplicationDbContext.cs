﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace GestionAchatErp.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<ApplicationUser> ApplicationUser { get; set; }
            
        public DbSet<GoodsReceivedNote> GoodsReceivedNote { get; set; }

        public DbSet<NumberSequence> NumberSequence { get; set; }

        public DbSet<PaymentReceive> PaymentReceive { get; set; }

        public DbSet<UnitOfMeasure> UnitOfMeasure { get; set; }

        public DbSet<UserProfile> UserProfile { get; set; }

        //new tables
        public DbSet<StatutWorkflow> StatutWorkflows { get; set; }
        public DbSet<DemandeAchatNature> DemandeAchatNatures { get; set; }
        public DbSet<DemandeAchat> DemandeAchats { get; set; }
        public DbSet<DemandeAchatItem> DemandeAchatItems { get; set; }
        public DbSet<HistoriqueStatut> HistoriqueStatut { get; set; }
        public DbSet<DemandeAchatStatutHistory> DemandeAchatStatutHistories { get; set; }
        public DbSet<Direction> Direction { get; set; }
        public DbSet<Departement> Departement { get; set; }
        public DbSet<Fonction> Fonction { get; set; }
        public DbSet<GestionAchatErp.Models.ExpressionBesoin> ExpressionBesoin { get; set; }
        public DbSet<GestionAchatErp.Models.ItemsExpressionBesoin> ItemsExpressionBesoin { get; set; }
        public DbSet<GestionAchatErp.Models.CategorieItems> CategorieItems { get; set; }
        public DbSet<GestionAchatErp.Models.Items> Items { get; set; }
        public DbSet<GestionAchatErp.Models.Signature> Signature { get; set; }
        public DbSet<GestionAchatErp.Models.Statuts> Statuts { get; set; }
        public DbSet<GestionAchatErp.Models.CentreBudgetaire> CentreBudgetaire { get; set; }
        public DbSet<GestionAchatErp.Models.TypeMarche> TypeMarche { get; set; }
        public DbSet<GestionAchatErp.Models.ItemsDemandeAchat> ItemsDemandeAchat { get; set; }

    }
}
