﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Helpers;
using Microsoft.AspNetCore.Identity;

namespace GestionAchatErp.Controllers
{
    public class ItemsExpressionBesoinsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public ItemsExpressionBesoinsController(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: ItemsExpressionBesoins
        public async Task<IActionResult> Index()
        {
            var itemsExpressionBesoins = _context.ItemsExpressionBesoin
                .Include(i => i.ExpressionBesoin);
            return View(await itemsExpressionBesoins.ToListAsync());
        }

        public async Task<IActionResult> List(int id)
        {
            var expressionBesoin = await _context.ExpressionBesoin.FindAsync(id);
            if (expressionBesoin == null)
            {
                return NotFound();
            }

            return View(expressionBesoin);
        }

        public async Task<IActionResult> ListSuivi(int id)
        {
            var expressionBesoin = await _context.ExpressionBesoin.FindAsync(id);
            if (expressionBesoin == null)
            {
                return NotFound();
            }

            return View(expressionBesoin);
        }

        
        [HttpPost]
        public JsonResult GetHistoriqueStatut(int? id)
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
           
            var data = _context.HistoriqueStatut.Where(a => a.ElementId == id && a.Element == ElementConts.ExpressionBesoin).AsQueryable();
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            if (!string.IsNullOrEmpty(searchValue))
            {
                //data = data.Where(x => x.Reference.ToLower().Contains(searchValue.ToLower()) || x.Designation.ToLower().Contains(searchValue.ToLower()) || x.CategorieItemLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var dataList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = dataList
            };

            return Json(returnObj);
        }

        [HttpPost]
        public JsonResult GetItemExpressionBesoinsList(int? id)
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.ItemsExpressionBesoin.Where(a => a.ExpressionBesoinId == id).AsQueryable();
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.Reference.ToLower().Contains(searchValue.ToLower()) || x.Designation.ToLower().Contains(searchValue.ToLower()) || x.CategorieItemLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var dataList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = dataList
            };

            return Json(returnObj);
        }


        // GET: ItemsExpressionBesoins/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.ItemsExpressionBesoin == null)
            {
                return NotFound();
            }

            var itemsExpressionBesoin = await _context.ItemsExpressionBesoin
                .Include(i => i.ExpressionBesoin)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemsExpressionBesoin == null)
            {
                return NotFound();
            }

            return View(itemsExpressionBesoin);
        }

        //Soumettre une expression
        public async Task<IActionResult> SoumettreExpressionBesoin(int? id)
        {
            if (id == null)
                return NotFound();

            var expression = await _context.ExpressionBesoin.FindAsync(id);
            if (expression == null)
                return NotFound();
            
            return PartialView("Soumettre", expression);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SoumettreExpressionBesoin(int id)
        {
             
            try
            {
                var expressionBesoin = await _context.ExpressionBesoin.FindAsync(id);
                if (expressionBesoin == null)
                    return NotFound();


                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                //Historiser le statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.ExpressionBesoin;
                historique.ElementId = expressionBesoin.Id;
                historique.Statut = expressionBesoin.DernierStatut;
                historique.DateDebutStatut = expressionBesoin.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = expressionBesoin.Commentaire;
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);

                expressionBesoin.DernierStatut = StatutConts.SOUMETTRE;
                expressionBesoin.DateStatut = DateTime.Now;
                _context.Update(expressionBesoin);
                await _context.SaveChangesAsync();
                return Content(expressionBesoin.DernierStatut);
            }
            catch (Exception )
            {
                //throw;
            }
            return Content("NOK");
        }

        //Soumettre une expression
        public async Task<IActionResult> RejeterExpressionBesoin(int? id)
        {
            if (id == null)
                return NotFound();

            var expression = await _context.ExpressionBesoin.FindAsync(id);
            if (expression == null)
                return NotFound();

            return PartialView("Rejeter", expression);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RejeterExpressionBesoin(int id)
        {

            try
            {
                var expressionBesoin = await _context.ExpressionBesoin.FindAsync(id);
                if (expressionBesoin == null)
                    return NotFound();


                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                //Historiser le statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.ExpressionBesoin;
                historique.ElementId = expressionBesoin.Id;
                historique.Statut = expressionBesoin.DernierStatut;
                historique.DateDebutStatut = expressionBesoin.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = expressionBesoin.Commentaire;
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);

                expressionBesoin.DernierStatut = StatutConts.REJETER;
                expressionBesoin.DateStatut = DateTime.Now;
                _context.Update(expressionBesoin);
                await _context.SaveChangesAsync();
                return Content(expressionBesoin.DernierStatut);
            }
            catch (Exception)
            {
                //throw;
            }
            return Content("NOK");
        }

        
         public async Task<IActionResult> ApprouvResCrbExpressionBesoin(int? id)
        {
            if (id == null)
                return NotFound();

            var expression = await _context.ExpressionBesoin.FindAsync(id);
            if (expression == null)
                return NotFound();
            var ListDepartement = _context.Departement.Select(d => new { IdDepartement = d.Id, LibelleDepartement = d.Libelle + " | " + d.DirectionLibelle });
            ViewData["CrbEnCharge"] = new SelectList(ListDepartement, "IdDepartement", "LibelleDepartement");
            var ListSourceBudgetaire = _context.CentreBudgetaire.Select(c => new { IdCentreBudgetaire = c.Id, LibelleCB = c.LibelleCb });
            ViewData["CentreBudgetaire"] = new SelectList(ListSourceBudgetaire, "IdCentreBudgetaire", "LibelleCB");
            return PartialView("ValiderRespCrb", expression);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApprouvResCrbExpressionBesoin(int id, ExpressionBesoin expressionBesoin)
        {
            try
            {
                var expression = await _context.ExpressionBesoin.FindAsync(id);
                if (expression == null)
                    return NotFound();

                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                //Historiser le statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.ExpressionBesoin;
                historique.ElementId = expression.Id;
                historique.Statut = expression.DernierStatut;
                historique.DateDebutStatut = expressionBesoin.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = expressionBesoin.Commentaire;
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);

                expression.Commentaire = expressionBesoin.Commentaire;
                expression.DernierStatut = StatutConts.VALIDRESPCRB;
                expression.SourceBudgetaire = expressionBesoin.SourceBudgetaire;
                expression.DateStatut = DateTime.Now;

                _context.Update(expression);
                await _context.SaveChangesAsync();
                return Content(expression.DernierStatut);
            }
            catch (Exception)
            {
                //throw;
            }
            return Content("NOK");
        }

        //Approbation Point Focal Crb
        public async Task<IActionResult> ApprouvPfCrbExpressionBesoin(int? id)
        {
            if (id == null)
                return NotFound();

            var expression = await _context.ExpressionBesoin.FindAsync(id);
            if (expression == null)
                return NotFound();
            var ListDepartement = _context.Departement.Select(d => new { IdDepartement = d.Id, LibelleDepartement = d.Libelle + " | " + d.DirectionLibelle });
            ViewData["CrbEnCharge"] = new SelectList(ListDepartement, "IdDepartement", "LibelleDepartement");
            var ListSourceBudgetaire = _context.CentreBudgetaire.Select(c => new { IdCentreBudgetaire = c.Id, LibelleCB = c.LibelleCb });
            ViewData["CentreBudgetaire"] = new SelectList(ListSourceBudgetaire, "IdCentreBudgetaire", "LibelleCB");
            return PartialView("ValiderPfCrb", expression);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ApprouvPfCrbExpressionBesoin(int id, ExpressionBesoin expressionBesoin)
        {
            try
            {
                var expression = await _context.ExpressionBesoin.FindAsync(id);
                if (expression == null)
                    return NotFound();

                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                //Historiser le statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.ExpressionBesoin;
                historique.ElementId = expression.Id;
                historique.Statut = expression.DernierStatut;
                historique.DateDebutStatut = expressionBesoin.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = expressionBesoin.Commentaire;
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);

                expression.Commentaire = expressionBesoin.Commentaire;
                expression.DernierStatut = StatutConts.VALIDCRB;
                expression.SourceBudgetaire = expressionBesoin.SourceBudgetaire;
                expression.DateStatut = DateTime.Now;

                _context.Update(expression);
                await _context.SaveChangesAsync();
                return Content(expression.DernierStatut);
            }
            catch (Exception)
            {
                //throw;
            }
            return Content("NOK");
        }


        //ValiderExpressionBesoin
        public async Task<IActionResult> ValiderExpressionBesoin(int? id)
        {
            if (id == null)
                return NotFound();

            var expression = await _context.ExpressionBesoin.FindAsync(id);
            if (expression == null)
                return NotFound();
            var ListDepartement = _context.Departement.Select(d => new {IdDepartement=d.Id,LibelleDepartement=d.Libelle+" | "+d.DirectionLibelle});
            ViewData["CrbEnCharge"] = new SelectList(ListDepartement, "IdDepartement", "LibelleDepartement");
            var ListSourceBudgetaire = _context.CentreBudgetaire.Select(c => new { IdCentreBudgetaire = c.Id, LibelleCB = c.LibelleCb});
            ViewData["CentreBudgetaire"] = new SelectList(ListSourceBudgetaire, "IdCentreBudgetaire", "LibelleCB");
            return PartialView("Valider", expression);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ValiderExpressionBesoin(int id, ExpressionBesoin expressionBesoin)
        {
            try
            {
                var expression = await _context.ExpressionBesoin.FindAsync(id);
                if (expression == null)
                    return NotFound();

                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                //Historiser le statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.ExpressionBesoin;
                historique.ElementId = expression.Id;
                historique.Statut = expression.DernierStatut;
                historique.DateDebutStatut = expressionBesoin.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = expressionBesoin.Commentaire;
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);

                expression.Commentaire = expressionBesoin.Commentaire;
                expression.Observation = expressionBesoin.Observation;
                expression.DernierStatut = StatutConts.VALIDRDEMANDEUR;
                expression.CrbEnCharge=expressionBesoin.CrbEnCharge;
                expression.SourceBudgetaire = expressionBesoin.SourceBudgetaire;
                expression.DateStatut = DateTime.Now;

                _context.Update(expression);
                await _context.SaveChangesAsync();
                return Content(expression.DernierStatut);
            }
            catch (Exception )
            {
                //throw;
            }
            return Content("NOK");
        }





        // GET: ItemsExpressionBesoins/Create
        public IActionResult Create(int expressionBesoinId)
        {
            var itemsIds  =  _context.ItemsExpressionBesoin.Where(a => a.ExpressionBesoinId == expressionBesoinId).Select(b => b.ItemId).ToList();

            var produitList = _context.Items.Where(c => !itemsIds.Contains(c.Id))
                .Select(p => new { CodeProduit = p.Id, LibelleProduit = p.Reference + " | " + p.Designation });
            ViewData["produitList"] = new SelectList(produitList, "CodeProduit", "LibelleProduit");

            return PartialView();
        }

        // POST: ItemsExpressionBesoins/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int expressionBesoinId, ItemsExpressionBesoin itemsExpressionBesoin)
        {
            if (ModelState.IsValid)
            {
                var expressionBesoin = await _context.ExpressionBesoin.FindAsync(expressionBesoinId);
                var produit =  await _context.Items.Include(z => z.CategorieItems).FirstOrDefaultAsync(q => q.Id == itemsExpressionBesoin.ItemId);

                itemsExpressionBesoin.ExpressionBesoinId = expressionBesoin.Id;
                itemsExpressionBesoin.ExpressionBesoin = expressionBesoin;
                itemsExpressionBesoin.CategorieItemId = produit.CategorieItemsId;
                itemsExpressionBesoin.CategorieItemLibelle = produit.CategorieItems?.Designation;
                itemsExpressionBesoin.Designation = produit.Designation;
                itemsExpressionBesoin.Reference = produit.Reference;

                _context.Add(itemsExpressionBesoin);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (Exception )
                {

                   // throw e;
                }
                return Content("OK");
            }
            return Content("NOK");
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.ItemsExpressionBesoin == null)
            {
                return NotFound();
            }

            var itemsExpressionBesoin = await _context.ItemsExpressionBesoin.FindAsync(id);
            if (itemsExpressionBesoin == null)
            {
                return NotFound();
            }
            ViewData["ExpressionBesoinId"] = new SelectList(_context.ExpressionBesoin, "Id", "Id", itemsExpressionBesoin.ExpressionBesoinId);
            ViewData["ItemsId"] = new SelectList(_context.Set<Items>(), "Id", "Id", itemsExpressionBesoin.ItemId);
            return View(itemsExpressionBesoin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ItemsExpressionBesoin itemsExpressionBesoin)
        {
            if (id != itemsExpressionBesoin.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(itemsExpressionBesoin);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemsExpressionBesoinExists(itemsExpressionBesoin.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ExpressionBesoinId"] = new SelectList(_context.ExpressionBesoin, "Id", "Id", itemsExpressionBesoin.ExpressionBesoinId);
            ViewData["ItemsId"] = new SelectList(_context.Set<Items>(), "Id", "Id", itemsExpressionBesoin.ItemId);
            return View(itemsExpressionBesoin);
        }

        // GET: ItemsExpressionBesoins/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.ItemsExpressionBesoin == null)
            {
                return NotFound();
            }

            var itemsExpressionBesoin = await _context.ItemsExpressionBesoin
                .Include(i => i.ExpressionBesoin)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemsExpressionBesoin == null)
            {
                return NotFound();
            }

            return View(itemsExpressionBesoin);
        }

        // POST: ItemsExpressionBesoins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.ItemsExpressionBesoin == null)
            {
                return Problem("Entity set 'ApplicationDbContext.ItemsExpressionBesoin'  is null.");
            }
            var itemsExpressionBesoin = await _context.ItemsExpressionBesoin.FindAsync(id);
            if (itemsExpressionBesoin != null)
            {
                _context.ItemsExpressionBesoin.Remove(itemsExpressionBesoin);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemsExpressionBesoinExists(int id)
        {
          return _context.ItemsExpressionBesoin.Any(e => e.Id == id);
        }


        //Modifier une expression de besoin
        [HttpGet("ItemsExpressionBesoins/EditItemsExpressionBesoin/{expressionBesoinId}/{id}")]
        public async Task<IActionResult> EditItemsExpressionBesoin(int expressionBesoinId, int id)
        {
            
            var expression = await _context.ItemsExpressionBesoin.FirstOrDefaultAsync(x =>x.ExpressionBesoinId == expressionBesoinId && x.Id == id );
            if (expression == null)
            {
                return NotFound();
            }

            return PartialView("EditProduct", expression);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ItemsExpressionBesoinUpdate([Bind("Id, ExpressionBesoinId, Designation, Quantite")]ItemsExpressionBesoin itemsExpressionBesoin)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    ItemsExpressionBesoin item = _context.ItemsExpressionBesoin.SingleOrDefault(x => x.Id == itemsExpressionBesoin.Id && x.ExpressionBesoinId == itemsExpressionBesoin.ExpressionBesoinId);
                    if (item != null)
                    {
                        item.Quantite = itemsExpressionBesoin.Quantite;
                        _context.ItemsExpressionBesoin.Update(item);
                        await _context.SaveChangesAsync();
                    } 
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemsExpressionBesoinExists(itemsExpressionBesoin.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json("NOK");
            }
            return Json("OK");
        }

        //Modifier une expression de besoin
        [HttpGet("ItemsExpressionBesoins/DeleteItemsExpressionBesoin/{expressionBesoinId}/{id}")]
        public async Task<IActionResult> DeleteItemsExpressionBesoin(int expressionBesoinId, int id)
        {

            var expression = await _context.ItemsExpressionBesoin.FirstOrDefaultAsync(x => x.ExpressionBesoinId == expressionBesoinId && x.Id == id);
            if (expression == null)
            {
                return NotFound();
            }
            if (expression != null)
            {
                _context.ItemsExpressionBesoin.Remove(expression);
                await _context.SaveChangesAsync();
                return Content("OK");
            }
            else
            {
                return Content("NOK");
            }
        }
    }
}
