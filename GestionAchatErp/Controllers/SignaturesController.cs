﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using Microsoft.AspNetCore.Identity;
using static GestionAchatErp.Pages.MainMenu;
using GestionAchatErp.Services;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;
using GestionAchatErp.Models.SyncfusionViewModels;
using GestionAchatErp.Models.Request;

namespace GestionAchatErp.Controllers
{
    public class SignaturesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IFunctional _functionalService;
        private readonly IHostingEnvironment _env;


        public SignaturesController(IFunctional functionalService,
            IHostingEnvironment env,
            ApplicationDbContext context, 
                UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            _functionalService = functionalService;
            _env = env;
        }
         
        // GET: Signatures
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Signature.Include(e => e.UserProfile);
            return View(await applicationDbContext.ToListAsync());
        }

        [HttpPost]
        public JsonResult GetSignaturesList()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<Signature>().AsQueryable();
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.AuteurSignature.ToLower().Contains(searchValue.ToLower()) || x.DirectionId.Equals(searchValue.ToLower()) || x.DepartementId.Equals(searchValue.ToLower()));
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return Json(returnObj);
        }

        // GET: Signatures/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Signature == null)
            {
                return NotFound();
            }

            var signature = await _context.Signature
                .Include(s => s.UserProfile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (signature == null)
            {
                return NotFound();
            }

            return View(signature);
        }

        // GET: Signatures/Create
        public IActionResult Create()
        {
            ViewData["UserProfile"] = new SelectList(_context.UserProfile, "UserProfileId", "FirstName");
            return PartialView("Create");
        }

        // POST: Signatures/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RequestSizeLimit(5000000)]
        public async Task<IActionResult> Create(SignatureRequest signature)
        {
            if (ModelState.IsValid)
            {
                var files = new List<IFormFile>();
                files.Add(signature.SignaturePath);
                var model = new Signature();

                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
                var folderUpload = "upload-signatures";
                var fileName = await _functionalService.UploadFile(files, _env, folderUpload);
              
                if (userProfile != null)
                {
                    var direction = _context.Direction.FirstOrDefault(x => x.Id == userProfile.DirectionId);
                    var departement = _context.Departement.FirstOrDefault(x => x.Id == userProfile.DepartementId);

                    model.UserProfileId = userProfile.UserProfileId;
                    model.UserProfile = userProfile;
                    model.DirectionId = userProfile.DirectionId;
                    model.DirectionLibelle = direction.Libelle;
                    model.DepartementLibelle= departement.Libelle;
                    model.DepartementId = userProfile.DepartementId;
                    model.SignaturePath = "/" + folderUpload + "/" + fileName;

                }
                model.AuteurSignature = signature.AuteurSignature;
                model.DateSignature = DateTime.Now;
                _context.Add(model);
                await _context.SaveChangesAsync();
                return Content("OK");
            }
            return Content("NOK");
        }

        // GET: Signatures/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Signature == null)
            {
                return NotFound();
            }

            var signature = await _context.Signature.FindAsync(id);
            if (signature == null)
            {
                return NotFound();
            }
            ViewData["UserProfileId"] = new SelectList(_context.UserProfile, "UserProfileId", "UserProfileId", signature.UserProfileId);
            return View(signature);
        }

        // POST: Signatures/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserProfileId,DirectionId,DepartementId,SignaturePath,AuteurSignature,DateSignature")] Signature signature)
        {
            if (id != signature.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(signature);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SignatureExists(signature.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserProfileId"] = new SelectList(_context.UserProfile, "UserProfileId", "UserProfileId", signature.UserProfileId);
            return View(signature);
        }

        // GET: Signatures/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Signature == null)
            {
                return NotFound();
            }

            var signature = await _context.Signature.FindAsync(id);

            if (signature == null)
            {
                return NotFound();
            }

            if (signature != null)
            {
                _context.Signature.Remove(signature);
                await _context.SaveChangesAsync();
                return Content("OK");
            }
            else
            {
                return Content("NOK");
            }

            
        }

        // POST: Signatures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Signature == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Signature'  is null.");
            }
            var signature = await _context.Signature.FindAsync(id);
            if (signature != null)
            {
                _context.Signature.Remove(signature);
            }
            
            await _context.SaveChangesAsync();
            return Content("OK");
        }

        private bool SignatureExists(int id)
        {
          return _context.Signature.Any(e => e.Id == id);
        }
    }
}
