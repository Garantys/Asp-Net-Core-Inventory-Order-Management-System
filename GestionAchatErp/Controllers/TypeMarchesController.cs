﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;

namespace GestionAchatErp.Controllers
{
    public class TypeMarchesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TypeMarchesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: TypeMarches
        public async Task<IActionResult> Index()
        {
            List <TypeMarche> model = await _context.TypeMarche.ToListAsync();
            return View(model);
        }

        // GET: TypeMarches/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TypeMarche == null)
            {
                return NotFound();
            }

            var typeMarche = await _context.TypeMarche
                .FirstOrDefaultAsync(m => m.Id == id);
            if (typeMarche == null)
            {
                return NotFound();
            }

            return View(typeMarche);
        }

        // GET: TypeMarches/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TypeMarches/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Code,Libelle")] TypeMarche typeMarche)
        {
            if (ModelState.IsValid)
            {
                _context.Add(typeMarche);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(typeMarche);
        }

        // GET: TypeMarches/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TypeMarche == null)
            {
                return NotFound();
            }

            var typeMarche = await _context.TypeMarche.FindAsync(id);
            if (typeMarche == null)
            {
                return NotFound();
            }
            return View(typeMarche);
        }

        // POST: TypeMarches/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Code,Libelle")] TypeMarche typeMarche)
        {
            if (id != typeMarche.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(typeMarche);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TypeMarcheExists(typeMarche.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(typeMarche);
        }

        // GET: TypeMarches/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TypeMarche == null)
            {
                return NotFound();
            }

            var typeMarche = await _context.TypeMarche
                .FirstOrDefaultAsync(m => m.Id == id);
            if (typeMarche == null)
            {
                return NotFound();
            }

            return View(typeMarche);
        }

        // POST: TypeMarches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TypeMarche == null)
            {
                return Problem("Entity set 'ApplicationDbContext.TypeMarche'  is null.");
            }
            var typeMarche = await _context.TypeMarche.FindAsync(id);
            if (typeMarche != null)
            {
                _context.TypeMarche.Remove(typeMarche);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TypeMarcheExists(int id)
        {
          return _context.TypeMarche.Any(e => e.Id == id);
        }
    }
}
