﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GestionAchatErp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using static GestionAchatErp.Pages.MainMenu;

namespace GestionAchatErp.Controllers
{
    
    public class UserRoleController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserRoleController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }


        [Authorize(Roles = Pages.MainMenu.User.RoleName)]
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Roles = Pages.MainMenu.ChangePassword.RoleName)]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [Authorize(Roles = Pages.MainMenu.Role.RoleName)]
        public IActionResult Role()
        {
            return View();
        }

        [Authorize(Roles = Pages.MainMenu.ChangeRole.RoleName)]
        public IActionResult ChangeRole()
        {
            return View();
        }

        [Authorize]
        public async Task<IActionResult> UserProfile()
        {
            ApplicationUser user = await _userManager.GetUserAsync(User);

            return View(user);
        }
        /*
        public async Task<IActionResult> UserProfilePicMethod(string? id)
        {
            var user = await userManager.FindByIdAsync(id);
            var profilPicName = user.ProfilePicName;
            if (profilPicName != null)
            {
                //I upload only jpg files. So my method is for jpg.
                //You can customize as per your requirements.
                var ext = ".jpg";
            
                //Get path for your file by path.combine filename+wwwpath+ext
                var fullname = Path.Combine(profilPicName, ext);
                string profilePicPath = Path.Combine(webHostEnvironment.WebRootPath, "images", fullname);
                //return the image file path
                return PhysicalFile(profilePicPath, "image/jpg");
            }
            //If no image exists return a placeholder image
            var profilePicPathNull = Path.Combine(webHostEnvironment.WebRootPath, "images", "placeholder.jpg");
            return PhysicalFile(profilePicPathNull, "image/jpg");
        } 

         */
    }
}