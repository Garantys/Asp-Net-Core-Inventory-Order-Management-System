﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json.Linq;
using GestionAchatErp.Helpers;

namespace GestionAchatErp.Controllers
{
    public class DemandeAchatsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;


        public DemandeAchatsController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: DemandeAchats
        public async Task<IActionResult> Index()
        {
              return View(await _context.DemandeAchats.ToListAsync());
        }

        // A valider Responsable RAFE
        public async Task<IActionResult> ValidationsRafe()
        {
            return View("ValidationRespRafe", await _context.DemandeAchats.ToListAsync());
        }

        // GET: DemandeAchats/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.DemandeAchats == null)
            {
                return NotFound();
            }

            var demandeAchat = await _context.DemandeAchats
                .FirstOrDefaultAsync(m => m.DemandeAchatId == id);
            if (demandeAchat == null)
            {
                return NotFound();
            }

            return View(demandeAchat);
        }
        
         [HttpPost]
        public async Task<JsonResult> GetDemandesAchatsAValider()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<DemandeAchat>().AsQueryable();
            ApplicationUser user = await _userManager.GetUserAsync(User);
            UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            data = data.Where(x => x.DirectionId.Equals(userProfile.DirectionId) && x.DepartementId.Equals(userProfile.DepartementId) && x.DernierStatut.ToLower().Contains("SOUMETTRE"));
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.DemandeurNom.ToLower().Contains(searchValue.ToLower()) || x.Direction.ToLower().Contains(searchValue.ToLower()) || x.Departement.ToLower().Contains(searchValue.ToLower()));
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return Json(returnObj);

        }


        [HttpPost]
        public async Task<JsonResult> GetDemandesAchatsList()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<DemandeAchat>().AsQueryable();
            ApplicationUser user = await _userManager.GetUserAsync(User);
            UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            //data = data.Where(x => x.UserId.Equals(userProfile.UserProfileId) && x.DirectionId.Equals(userProfile.DirectionId) && x.DepartementId.Equals(userProfile.DepartementId));
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.DemandeurNom.ToLower().Contains(searchValue.ToLower()) || x.Direction.ToLower().Contains(searchValue.ToLower()) || x.Departement.ToLower().Contains(searchValue.ToLower()));
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return Json(returnObj);

        }


        
            [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DemandeAchatUpdate([Bind("DemandeAchatId, TypeAchat, TypeMarche, DemandeMac")] DemandeAchat demandeAchat)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    DemandeAchat achat = _context.DemandeAchats.SingleOrDefault(x => x.DemandeAchatId == demandeAchat.DemandeAchatId);
                    if (achat != null)
                    {
                        achat.TypeAchat = demandeAchat.TypeAchat;
                        achat.TypeMarche = demandeAchat.TypeMarche;
                        achat.DemandeMac = demandeAchat.DemandeMac;
                        _context.DemandeAchats.Update(achat);
                        await _context.SaveChangesAsync();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DemandeAchatExists(demandeAchat.DemandeAchatId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json("NOK");
            }
            return Json("OK");
        }

        public async Task<IActionResult> SoumettreDemandeAchat(int? id)
        {
            try
            {
                var demandeAchat = await _context.DemandeAchats.FindAsync(id);
                if (demandeAchat == null)
                    return NotFound();


                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                //Historiser le statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.DemandeAchat;
                historique.ElementId = demandeAchat.DemandeAchatId;
                historique.Statut = demandeAchat.DernierStatut;
                historique.DateDebutStatut = (DateTime)demandeAchat.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = "demande achat soumise";
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);

                demandeAchat.DernierStatut = StatutConts.SOUMETTREPFRAFE;
                demandeAchat.DateStatut = DateTime.Now;
                _context.Update(demandeAchat);
                await _context.SaveChangesAsync();
                return Content(demandeAchat.DernierStatut);
            }
            catch (Exception)
            {
                //throw;
            }
            return Content("NOK");
        }

        // GET: DemandeAchats/Create/{?id}
        public async Task<IActionResult> Create(int? id)
        {
            ExpressionBesoin expressionBesoin = await _context.ExpressionBesoin.FirstOrDefaultAsync(x => x.Id == id);

            var itemsD = await _context.ItemsExpressionBesoin.Where(x => x.ExpressionBesoinId == expressionBesoin.Id).ToListAsync();
            var demandeAchat = new DemandeAchat();

            if (expressionBesoin != null)
            {
                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                demandeAchat.DirectionId = expressionBesoin.DirectionId;
                demandeAchat.Direction = expressionBesoin.DirectionLibelle;
                demandeAchat.DepartementId = expressionBesoin.DepartementId;
                demandeAchat.Departement = expressionBesoin.DepartementLibelle;
                demandeAchat.ExpressionBesoinId = expressionBesoin.Id;
                demandeAchat.NumDemande = expressionBesoin.NumExpressionBesoin;
                demandeAchat.MotifDemande = expressionBesoin.MotifDemande;
                demandeAchat.DemandeurId = expressionBesoin.UserProfileId.ToString();
                demandeAchat.DemandeurNom = expressionBesoin.NomDemandeur;
                demandeAchat.NatureBesoin = expressionBesoin.NatureBesoin;
                demandeAchat.DernierStatut = StatutConts.CREERPFRAFE ;
                demandeAchat.DateStatut = DateTime.Today;
                demandeAchat.DateDemande = DateTime.Today;
                demandeAchat.UserId = int.Parse(user.Id);

                //historique statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.ExpressionBesoin;
                historique.ElementId = expressionBesoin.Id;
                historique.Statut = expressionBesoin.DernierStatut;
                historique.DateDebutStatut = expressionBesoin.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = expressionBesoin.Commentaire;
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);


                expressionBesoin.DernierStatut = StatutConts.TRANSMDA;
                expressionBesoin.DateStatut = DateTime.Now;
                _context.Update(expressionBesoin);
            }
            _context.Add(demandeAchat);
            var status = await _context.SaveChangesAsync();
            if(status > 0)
            {
                if (itemsD.Count > 0)
                {
                    foreach (var item in itemsD)
                    {
                        var itemsDemandeAchat = new ItemsDemandeAchat();

                        itemsDemandeAchat.DemandeAchatId = demandeAchat.DemandeAchatId;
                        itemsDemandeAchat.CategorieItemId = item.CategorieItemId;
                        itemsDemandeAchat.CategorieItemLibelle = item.CategorieItemLibelle;
                        itemsDemandeAchat.ItemId = item.ItemId;
                        itemsDemandeAchat.Designation = item.Designation;
                        itemsDemandeAchat.Reference = item.Reference;
                        itemsDemandeAchat.Quantite = item.Quantite;
                        _context.Add(itemsDemandeAchat);
                        await _context.SaveChangesAsync();
                    }
                }

            }
            //return Json(status);
            return Content(demandeAchat.DemandeAchatId.ToString());

            //return View(demandeAchat);
        }

        // POST: DemandeAchats/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DemandeAchatId,UserId,UserResponsableId,UserResponsableCrbId,DirectionId,DepartementId,FonctionId,DemandeurId,DemandeurNom,ExpressionBesoinId,Direction,Departement,Fonction,NumDemande,CrbEnCharge,TypeAchat,TypeMarche,DemandeMac,NatureBesoin,MotifDemande,ImputationBudgetaire,CreditAlloue,ModificationBudgetaire,EngagementAnterieur,CreditDisponible,DateDemande,DernierStatut,DateStatut,Observation,Commentaire,DemandeAchatNatureId")] DemandeAchat demandeAchat)
        {
            if (ModelState.IsValid)
            {
                _context.Add(demandeAchat);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(demandeAchat);
        }

        // GET: DemandeAchats/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.DemandeAchats == null)
            {
                return NotFound();
            }

            var demandeAchat = await _context.DemandeAchats.FindAsync(id);
            if (demandeAchat == null)
            {
                return NotFound();
            }
            return View(demandeAchat);
        }
        // Afficher formulaire Modifier une expression de besoin
        public async Task<IActionResult> ModifierDemandeAchat(int? id)
        {
            if (id == null)
                return NotFound();

            var demande = await _context.DemandeAchats.FindAsync(id);
            if (demande == null)
                return NotFound();
            var ListDepartement = _context.Departement.Select(d => new { IdDepartement = d.Id, LibelleDepartement = d.Libelle + " | " + d.DirectionLibelle });
            ViewData["CrbEnCharge"] = new SelectList(ListDepartement, "IdDepartement", "LibelleDepartement");
            var ListSourceBudgetaire = _context.CentreBudgetaire.Select(c => new { IdCentreBudgetaire = c.Id, LibelleCB = c.LibelleCb });
            ViewData["CentreBudgetaire"] = new SelectList(ListSourceBudgetaire, "IdCentreBudgetaire", "LibelleCB");
            return View("Modifier", demande);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ModifierDemandeAchat(int id, string Commentaire, DemandeAchat demandeAchat)
        {
            try
            {
                var achat = await _context.DemandeAchats.FindAsync(id);
                if (demandeAchat == null)
                    return NotFound();

                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                achat.NatureBesoin = demandeAchat.NatureBesoin;
                achat.MotifDemande = demandeAchat.MotifDemande;
                achat.TypeAchat = demandeAchat.TypeAchat;
                achat.TypeMarche = demandeAchat.TypeMarche;
                //expression.DernierStatut = StatutConts.VALIDER;
                achat.DemandeMac = demandeAchat.DemandeMac;
                achat.CreditDisponible = demandeAchat.CreditDisponible;
                achat.ModificationBudgetaire = demandeAchat.ModificationBudgetaire;
                achat.EngagementAnterieur = demandeAchat.EngagementAnterieur;
                achat.CreditAlloue = demandeAchat.CreditAlloue;
                achat.CrbEnCharge = demandeAchat.CrbEnCharge;
                achat.ImputationBudgetaire = demandeAchat.ImputationBudgetaire;
                achat.Observation = demandeAchat.Observation;
                achat.Commentaire = demandeAchat.Commentaire;
                achat.DateStatut = DateTime.Now;

                _context.Update(achat);
                await _context.SaveChangesAsync();
                return Content(achat.DernierStatut);
            }
            catch (Exception)
            {
                //throw;
            }
            return Content("NOK");
        }


        // POST: DemandeAchats/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DemandeAchatId,UserId,UserResponsableId,UserResponsableCrbId,DirectionId,DepartementId,FonctionId,DemandeurId,DemandeurNom,ExpressionBesoinId,Direction,Departement,Fonction,NumDemande,CrbEnCharge,TypeAchat,TypeMarche,DemandeMac,NatureBesoin,MotifDemande,ImputationBudgetaire,CreditAlloue,ModificationBudgetaire,EngagementAnterieur,CreditDisponible,DateDemande,DernierStatut,DateStatut,Observation,Commentaire,DemandeAchatNatureId")] DemandeAchat demandeAchat)
        {
            if (id != demandeAchat.DemandeAchatId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(demandeAchat);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DemandeAchatExists(demandeAchat.DemandeAchatId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(demandeAchat);
        }

        // GET: DemandeAchats/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.DemandeAchats == null)
            {
                return NotFound();
            }

            var demandeAchat = await _context.DemandeAchats
                .FirstOrDefaultAsync(m => m.DemandeAchatId == id);
            if (demandeAchat == null)
            {
                return NotFound();
            }

            return View(demandeAchat);
        }

        // POST: DemandeAchats/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.DemandeAchats == null)
            {
                return Problem("Entity set 'ApplicationDbContext.DemandeAchats'  is null.");
            }
            var demandeAchat = await _context.DemandeAchats.FindAsync(id);
            if (demandeAchat != null)
            {
                _context.DemandeAchats.Remove(demandeAchat);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DemandeAchatExists(int id)
        {
          return _context.DemandeAchats.Any(e => e.DemandeAchatId == id);
        }


        public async Task<IActionResult> BesoinsEnAttente()
        {
            var applicationDbContext = _context.ExpressionBesoin.Include(e => e.UserProfile);
            return View("BesoinsEnAttente", await applicationDbContext.ToListAsync()); ;

        }

        [HttpPost]
        public async Task<JsonResult> GetExpressionBesoinsValides()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<ExpressionBesoin>().AsQueryable();
            ApplicationUser user = await _userManager.GetUserAsync(User);
            UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found

            data = data.Where(x => x.DirectionId.Equals(userProfile.DirectionId) && x.DepartementId.Equals(userProfile.DepartementId) && x.DernierStatut.ToLower().Contains("VALIDRESPCRB"));
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.NomDemandeur.ToLower().Contains(searchValue.ToLower()) || x.DirectionLibelle.ToLower().Contains(searchValue.ToLower()) || x.DepartementLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            data = data.OrderBy(x => x.DernierStatut);
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return Json(returnObj);
        }

        //ValiderDemandeAchat
        public async Task<IActionResult> ValiderDemandeAchat(int? id)
        {
            if (id == null)
                return NotFound();

            var demande = await _context.DemandeAchats.FindAsync(id);
            if (demande == null)
                return NotFound();
            
            return PartialView("Valider", demande);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ValiderDemandeAchat(int id, DemandeAchat demandeAchat)
        {
            try
            {
                var demande = await _context.DemandeAchats.FindAsync(id);
                if (demande == null)
                    return NotFound();

                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                //Historiser le statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.DemandeAchat;
                historique.ElementId = demande.DemandeAchatId;
                historique.Statut = demande.DernierStatut;
                historique.DateDebutStatut = (DateTime) demande.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = demandeAchat.Commentaire;
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);

                demande.Commentaire = demandeAchat.Commentaire;
                demande.Observation = demandeAchat.Observation;
                demande.DernierStatut = StatutConts.VALIDRAFE;
                demande.DateStatut = DateTime.Now;

                _context.Update(demande);
                await _context.SaveChangesAsync();
                return Content(demande.DernierStatut);
            }
            catch (Exception)
            {
                //throw;
            }
            return Content("NOK");
        }


        //REJETER DEMANDE ACHAT
        public async Task<IActionResult> RejeterDemandeAchat(int? id)
        {
            if (id == null)
                return NotFound();

            var demande = await _context.DemandeAchats.FindAsync(id);
            if (demande == null)
                return NotFound();

            return PartialView("Rejeter", demande);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RejeterDemandeAchat(int id, DemandeAchat demandeAchat)
        {

            try
            {
                var demande = await _context.DemandeAchats.FindAsync(id);
                if (demande == null)
                    return NotFound();


                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                //Historiser le statut
                HistoriqueStatut historique = new HistoriqueStatut();
                historique.Element = ElementConts.DemandeAchat;
                historique.ElementId = demande.DemandeAchatId;
                historique.Statut = demande.DernierStatut;
                historique.DateDebutStatut = (DateTime)demande.DateStatut;
                historique.DateFinStatut = DateTime.Now;
                historique.DateCreation = DateTime.Now;
                historique.Commentaire = demandeAchat.Commentaire;
                historique.UserProfileId = userProfile.UserProfileId;
                _context.Add(historique);

                demande.DernierStatut = StatutConts.REJETER;
                demande.DateStatut = DateTime.Now;
                _context.Update(demande);
                await _context.SaveChangesAsync();
                return Content(demande.DernierStatut);
            }
            catch (Exception)
            {
                //throw;
            }
            return Content("NOK");
        }

        


    }
}
