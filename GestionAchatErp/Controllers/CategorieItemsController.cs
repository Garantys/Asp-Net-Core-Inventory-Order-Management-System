﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;

namespace GestionAchatErp.Controllers
{
    public class CategorieItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategorieItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: CategorieItems
        public async Task<IActionResult> Index()
        {
            List<CategorieItems> model = await _context.CategorieItems.ToListAsync();

            return View(model);
        }

        // GET: CategorieItems/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.CategorieItems == null)
            {
                return NotFound();
            }

            var categorieItems = await _context.CategorieItems
                .FirstOrDefaultAsync(m => m.Id == id);
            if (categorieItems == null)
            {
                return NotFound();
            }

            return View(categorieItems);
        }

        // GET: CategorieItems/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CategorieItems/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Designation")] CategorieItems categorieItems)
        {
            if (ModelState.IsValid)
            {
                _context.Add(categorieItems);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(categorieItems);
        }

        // GET: CategorieItems/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.CategorieItems == null)
            {
                return NotFound();
            }

            var categorieItems = await _context.CategorieItems.FindAsync(id);
            if (categorieItems == null)
            {
                return NotFound();
            }
            return View(categorieItems);
        }

        // POST: CategorieItems/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Designation")] CategorieItems categorieItems)
        {
            if (id != categorieItems.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(categorieItems);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategorieItemsExists(categorieItems.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(categorieItems);
        }

        // GET: CategorieItems/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.CategorieItems == null)
            {
                return NotFound();
            }

            var categorieItems = await _context.CategorieItems
                .FirstOrDefaultAsync(m => m.Id == id);
            if (categorieItems == null)
            {
                return NotFound();
            }

            return View(categorieItems);
        }

        // POST: CategorieItems/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.CategorieItems == null)
            {
                return Problem("Entity set 'ApplicationDbContext.CategorieItems'  is null.");
            }
            var categorieItems = await _context.CategorieItems.FindAsync(id);
            if (categorieItems != null)
            {
                _context.CategorieItems.Remove(categorieItems);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CategorieItemsExists(int id)
        {
          return _context.CategorieItems.Any(e => e.Id == id);
        }
    }
}
