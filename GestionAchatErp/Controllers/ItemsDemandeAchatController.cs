﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Helpers;
using Microsoft.AspNetCore.Identity;


namespace GestionAchatErp.Controllers
{
    public class ItemsDemandeAchatController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        public ItemsDemandeAchatController(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        // GET: ItemsDemandeAchat
        public ActionResult Index()
        {
            return View();
        }
        public async Task<IActionResult> List(int id)
        {
            var demandeAchat = await _context.DemandeAchats.FindAsync(id);
            if (demandeAchat == null)
            {
                return NotFound();
            }

            return View(demandeAchat);
        }

        [HttpPost]
        public JsonResult GetItemsDemandesAchatsList(int? id)
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var demandeAchat = _context.DemandeAchats.Where(a => a.DemandeAchatId == id).FirstOrDefault();
            var data = _context.ItemsDemandeAchat.Where(a => a.DemandeAchatId == demandeAchat.DemandeAchatId).AsQueryable();
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.Reference.ToLower().Contains(searchValue.ToLower()) || x.Designation.ToLower().Contains(searchValue.ToLower()) || x.CategorieItemLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var dataList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = dataList
            };

            return Json(returnObj);
        }


        // GET: ItemsDemandeAchat/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //Modifier le prix Item demande Achat
        [HttpGet("ItemsDemandeAchat/EditPrixItemsDemandeAchat/{demandeAchatId}/{id}")]
        public async Task<IActionResult> EditItemsExpressionBesoin(int demandeAchatId, int id)
        {

            var expression = await _context.ItemsDemandeAchat.FirstOrDefaultAsync(x => x.DemandeAchatId == demandeAchatId && x.Id == id);
            if (expression == null)
            {
                return NotFound();
            }

            return PartialView("EditProduct", expression);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ItemsDemandeAchatUpdate([Bind("Id, DemandeAchatId, Designation, Quantite, PrixUnitaire, MontantTotal")] ItemsDemandeAchat itemsDemandeAchat)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    ItemsDemandeAchat item = _context.ItemsDemandeAchat.SingleOrDefault(x => x.Id == itemsDemandeAchat.Id && x.DemandeAchatId == itemsDemandeAchat.DemandeAchatId);
                    if (item != null)
                    {
                        item.PrixUnitaire = itemsDemandeAchat.PrixUnitaire;
                        item.MontantTotal = itemsDemandeAchat.PrixUnitaire*itemsDemandeAchat.Quantite;
                        _context.ItemsDemandeAchat.Update(item);
                        await _context.SaveChangesAsync();
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemsDemandeAchatExists(itemsDemandeAchat.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return Json("NOK");
            }
            return Json("OK");
        }

        // GET: ItemsDemandeAchat/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ItemsDemandeAchat/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: ItemsDemandeAchat/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: ItemsDemandeAchat/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        private bool ItemsDemandeAchatExists(int id)
        {
            return _context.ItemsDemandeAchat.Any(e => e.Id == id);
        }



        // GET: ItemsDemandeAchat/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: ItemsDemandeAchat/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
