﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;

namespace GestionAchatErp.Controllers
{
    public class CentreBudgetairesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CentreBudgetairesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: CentreBudgetaires
        public async Task<IActionResult> Index()
        {
            List <CentreBudgetaire> model = await _context.CentreBudgetaire.ToListAsync();
            return View(model);
        }

        // GET: CentreBudgetaires/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.CentreBudgetaire == null)
            {
                return NotFound();
            }

            var centreBudgetaire = await _context.CentreBudgetaire
                .FirstOrDefaultAsync(m => m.Id == id);
            if (centreBudgetaire == null)
            {
                return NotFound();
            }

            return View(centreBudgetaire);
        }

        // GET: CentreBudgetaires/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CentreBudgetaires/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Code,LibelleCb")] CentreBudgetaire centreBudgetaire)
        {
            if (ModelState.IsValid)
            {
                _context.Add(centreBudgetaire);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(centreBudgetaire);
        }

        // GET: CentreBudgetaires/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.CentreBudgetaire == null)
            {
                return NotFound();
            }

            var centreBudgetaire = await _context.CentreBudgetaire.FindAsync(id);
            if (centreBudgetaire == null)
            {
                return NotFound();
            }
            return View(centreBudgetaire);
        }

        // POST: CentreBudgetaires/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Code,LibelleCb")] CentreBudgetaire centreBudgetaire)
        {
            if (id != centreBudgetaire.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(centreBudgetaire);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CentreBudgetaireExists(centreBudgetaire.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(centreBudgetaire);
        }

        // GET: CentreBudgetaires/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.CentreBudgetaire == null)
            {
                return NotFound();
            }

            var centreBudgetaire = await _context.CentreBudgetaire
                .FirstOrDefaultAsync(m => m.Id == id);
            if (centreBudgetaire == null)
            {
                return NotFound();
            }

            return View(centreBudgetaire);
        }

        // POST: CentreBudgetaires/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.CentreBudgetaire == null)
            {
                return Problem("Entity set 'ApplicationDbContext.CentreBudgetaire'  is null.");
            }
            var centreBudgetaire = await _context.CentreBudgetaire.FindAsync(id);
            if (centreBudgetaire != null)
            {
                _context.CentreBudgetaire.Remove(centreBudgetaire);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CentreBudgetaireExists(int id)
        {
          return _context.CentreBudgetaire.Any(e => e.Id == id);
        }
    }
}
