﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;

namespace GestionAchatErp.Controllers
{
    public class StatutsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StatutsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Statuts
        public async Task<IActionResult> Index()
        {
            List <Statuts> model = await _context.Statuts.ToListAsync();
            return View(model);
        }

        // GET: Statuts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Statuts == null)
            {
                return NotFound();
            }

            var statuts = await _context.Statuts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (statuts == null)
            {
                return NotFound();
            }

            return View(statuts);
        }

        // GET: Statuts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Statuts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Etape,Libelle")] Statuts statuts)
        {
            if (ModelState.IsValid)
            {
                _context.Add(statuts);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(statuts);
        }

        // GET: Statuts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Statuts == null)
            {
                return NotFound();
            }

            var statuts = await _context.Statuts.FindAsync(id);
            if (statuts == null)
            {
                return NotFound();
            }
            return View(statuts);
        }

        // POST: Statuts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Etape,Libelle")] Statuts statuts)
        {
            if (id != statuts.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(statuts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StatutsExists(statuts.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(statuts);
        }

        // GET: Statuts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Statuts == null)
            {
                return NotFound();
            }

            var statuts = await _context.Statuts
                .FirstOrDefaultAsync(m => m.Id == id);
            if (statuts == null)
            {
                return NotFound();
            }

            return View(statuts);
        }

        // POST: Statuts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Statuts == null)
            {
                return Problem("Entity set 'ApplicationDbContext.Statuts'  is null.");
            }
            var statuts = await _context.Statuts.FindAsync(id);
            if (statuts != null)
            {
                _context.Statuts.Remove(statuts);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StatutsExists(int id)
        {
          return _context.Statuts.Any(e => e.Id == id);
        }
    }
}
