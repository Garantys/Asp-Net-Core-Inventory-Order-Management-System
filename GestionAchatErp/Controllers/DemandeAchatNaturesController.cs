﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;

namespace GestionAchatErp.Controllers
{
    public class DemandeAchatNaturesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DemandeAchatNaturesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DemandeAchatNatures
        public async Task<IActionResult> Index()
        {
              return View(await _context.DemandeAchatNatures.ToListAsync());
        }

        // GET: DemandeAchatNatures/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.DemandeAchatNatures == null)
            {
                return NotFound();
            }

            var demandeAchatNature = await _context.DemandeAchatNatures
                .FirstOrDefaultAsync(m => m.DemandeAchatNatureId == id);
            if (demandeAchatNature == null)
            {
                return NotFound();
            }

            return View(demandeAchatNature);
        }

        // GET: DemandeAchatNatures/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: DemandeAchatNatures/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DemandeAchatNatureId,Code,Libelle")] DemandeAchatNature demandeAchatNature)
        {
            if (ModelState.IsValid)
            {
                _context.Add(demandeAchatNature);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(demandeAchatNature);
        }

        // GET: DemandeAchatNatures/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.DemandeAchatNatures == null)
            {
                return NotFound();
            }

            var demandeAchatNature = await _context.DemandeAchatNatures.FindAsync(id);
            if (demandeAchatNature == null)
            {
                return NotFound();
            }
            return View(demandeAchatNature);
        }

        // POST: DemandeAchatNatures/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DemandeAchatNatureId,Code,Libelle")] DemandeAchatNature demandeAchatNature)
        {
            if (id != demandeAchatNature.DemandeAchatNatureId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(demandeAchatNature);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DemandeAchatNatureExists(demandeAchatNature.DemandeAchatNatureId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(demandeAchatNature);
        }

        // GET: DemandeAchatNatures/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.DemandeAchatNatures == null)
            {
                return NotFound();
            }

            var demandeAchatNature = await _context.DemandeAchatNatures
                .FirstOrDefaultAsync(m => m.DemandeAchatNatureId == id);
            if (demandeAchatNature == null)
            {
                return NotFound();
            }

            return View(demandeAchatNature);
        }

        // POST: DemandeAchatNatures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.DemandeAchatNatures == null)
            {
                return Problem("Entity set 'ApplicationDbContext.DemandeAchatNatures'  is null.");
            }
            var demandeAchatNature = await _context.DemandeAchatNatures.FindAsync(id);
            if (demandeAchatNature != null)
            {
                _context.DemandeAchatNatures.Remove(demandeAchatNature);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DemandeAchatNatureExists(int id)
        {
          return _context.DemandeAchatNatures.Any(e => e.DemandeAchatNatureId == id);
        }
    }
}
