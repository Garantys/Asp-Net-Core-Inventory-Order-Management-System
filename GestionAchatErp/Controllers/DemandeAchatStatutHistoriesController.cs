﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;

namespace GestionAchatErp.Controllers
{
    public class DemandeAchatStatutHistoriesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DemandeAchatStatutHistoriesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DemandeAchatStatutHistories
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.DemandeAchatStatutHistories.Include(d => d.DemandeAchat);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: DemandeAchatStatutHistories/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.DemandeAchatStatutHistories == null)
            {
                return NotFound();
            }

            var demandeAchatStatutHistory = await _context.DemandeAchatStatutHistories
                .Include(d => d.DemandeAchat)
                .FirstOrDefaultAsync(m => m.DemandeAchatStatutHistoryId == id);
            if (demandeAchatStatutHistory == null)
            {
                return NotFound();
            }

            return View(demandeAchatStatutHistory);
        }

        // GET: DemandeAchatStatutHistories/Create
        public IActionResult Create()
        {
            ViewData["DemandeAchatId"] = new SelectList(_context.DemandeAchats, "DemandeAchatId", "DemandeAchatId");
            return View();
        }

        // POST: DemandeAchatStatutHistories/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DemandeAchatStatutHistoryId,StatutName,OldStatutName,DateCreatedDebut,DateCreatedFin,Commentaire,DemandeAchatId")] DemandeAchatStatutHistory demandeAchatStatutHistory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(demandeAchatStatutHistory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["DemandeAchatId"] = new SelectList(_context.DemandeAchats, "DemandeAchatId", "DemandeAchatId", demandeAchatStatutHistory.DemandeAchatId);
            return View(demandeAchatStatutHistory);
        }

        // GET: DemandeAchatStatutHistories/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.DemandeAchatStatutHistories == null)
            {
                return NotFound();
            }

            var demandeAchatStatutHistory = await _context.DemandeAchatStatutHistories.FindAsync(id);
            if (demandeAchatStatutHistory == null)
            {
                return NotFound();
            }
            ViewData["DemandeAchatId"] = new SelectList(_context.DemandeAchats, "DemandeAchatId", "DemandeAchatId", demandeAchatStatutHistory.DemandeAchatId);
            return View(demandeAchatStatutHistory);
        }

        // POST: DemandeAchatStatutHistories/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("DemandeAchatStatutHistoryId,StatutName,OldStatutName,DateCreatedDebut,DateCreatedFin,Commentaire,DemandeAchatId")] DemandeAchatStatutHistory demandeAchatStatutHistory)
        {
            if (id != demandeAchatStatutHistory.DemandeAchatStatutHistoryId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(demandeAchatStatutHistory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DemandeAchatStatutHistoryExists(demandeAchatStatutHistory.DemandeAchatStatutHistoryId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["DemandeAchatId"] = new SelectList(_context.DemandeAchats, "DemandeAchatId", "DemandeAchatId", demandeAchatStatutHistory.DemandeAchatId);
            return View(demandeAchatStatutHistory);
        }

        // GET: DemandeAchatStatutHistories/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.DemandeAchatStatutHistories == null)
            {
                return NotFound();
            }

            var demandeAchatStatutHistory = await _context.DemandeAchatStatutHistories
                .Include(d => d.DemandeAchat)
                .FirstOrDefaultAsync(m => m.DemandeAchatStatutHistoryId == id);
            if (demandeAchatStatutHistory == null)
            {
                return NotFound();
            }

            return View(demandeAchatStatutHistory);
        }

        // POST: DemandeAchatStatutHistories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.DemandeAchatStatutHistories == null)
            {
                return Problem("Entity set 'ApplicationDbContext.DemandeAchatStatutHistories'  is null.");
            }
            var demandeAchatStatutHistory = await _context.DemandeAchatStatutHistories.FindAsync(id);
            if (demandeAchatStatutHistory != null)
            {
                _context.DemandeAchatStatutHistories.Remove(demandeAchatStatutHistory);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DemandeAchatStatutHistoryExists(int id)
        {
          return _context.DemandeAchatStatutHistories.Any(e => e.DemandeAchatStatutHistoryId == id);
        }
    }
}
