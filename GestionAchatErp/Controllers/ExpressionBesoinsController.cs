﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using static GestionAchatErp.Pages.MainMenu;
using SendGrid;
using Microsoft.AspNetCore.Identity;
using GestionAchatErp.Helpers;
using GestionAchatErp.Services;

namespace GestionAchatErp.Controllers
{
    public class ExpressionBesoinsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly INumberSequence _numberSequence;

        public ExpressionBesoinsController(ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            INumberSequence numberSequence)
        {
            _context = context;
            _userManager = userManager;
            _numberSequence = numberSequence;
        } 



        // GET: ExpressionBesoins
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.ExpressionBesoin.Include(e => e.UserProfile);
            return View(await applicationDbContext.ToListAsync());
        }
        public async Task<IActionResult> SuivreMesBesoins()
        {
            var applicationDbContext = _context.ExpressionBesoin.Include(e => e.UserProfile);
            return View("SuivreMesBesoins", await applicationDbContext.ToListAsync()); ;

        }

        
        [HttpPost]
        public async Task<JsonResult> GetMesExpressionBesoins()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<ExpressionBesoin>().AsQueryable();
            ApplicationUser user = await _userManager.GetUserAsync(User);
            UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            data = data.Where(x => x.UserProfileId.Equals(userProfile.UserProfileId) && x.DirectionId.Equals(userProfile.DirectionId) && x.DepartementId.Equals(userProfile.DepartementId)).OrderByDescending(x=> x.Id) ;
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.NomDemandeur.ToLower().Contains(searchValue.ToLower()) || x.DirectionLibelle.ToLower().Contains(searchValue.ToLower()) || x.DepartementLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return Json(returnObj);
        }
        // GET: ExpressionBesoins à valider
        public async Task<IActionResult> ListeValider()
        {
            var applicationDbContext = _context.ExpressionBesoin.Include(e => e.UserProfile);
            return View("ListeAValider",await applicationDbContext.ToListAsync());;
        }


        public async Task<IActionResult> ListeValiderCrb()
        {
            var applicationDbContext = _context.ExpressionBesoin.Include(e => e.UserProfile);
            return View("ListeAValiderCrb", await applicationDbContext.ToListAsync()); ;

        }

        public async Task<IActionResult> ListeValiderRCrb()
        {
            var applicationDbContext = _context.ExpressionBesoin.Include(e => e.UserProfile);
            return View("ListeAValiderRespCrb", await applicationDbContext.ToListAsync()); ;
        }

        [HttpPost]
        public async Task<JsonResult> GetExpressionBesoinsList()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<ExpressionBesoin>().AsQueryable();
            ApplicationUser user = await _userManager.GetUserAsync(User);
            UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            data = data.Where(x => x.UserProfileId.Equals(userProfile.UserProfileId) && x.DirectionId.Equals(userProfile.DirectionId) && x.DepartementId.Equals(userProfile.DepartementId) && x.DernierStatut == "CREER");
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.NomDemandeur.ToLower().Contains(searchValue.ToLower()) || x.DirectionLibelle.ToLower().Contains(searchValue.ToLower()) || x.DepartementLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return  Json(returnObj);
        }
        [HttpPost]
        public async Task<JsonResult> GetExpressionBesoinsListValider()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<ExpressionBesoin>().AsQueryable();
            ApplicationUser user = await _userManager.GetUserAsync(User);
            UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found
            
            data = data.Where(x =>  x.DirectionId.Equals(userProfile.DirectionId) && x.DepartementId.Equals(userProfile.DepartementId) && x.DernierStatut.ToLower().Contains("SOUMETTRE"));
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.NomDemandeur.ToLower().Contains(searchValue.ToLower()) || x.DirectionLibelle.ToLower().Contains(searchValue.ToLower()) || x.DepartementLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            data= data.OrderBy(x => x.DernierStatut);
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return Json(returnObj);
        }


        [HttpPost]
        public async Task<JsonResult> GetExpressionBesoinsListValiderCrb()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<ExpressionBesoin>().AsQueryable();
            ApplicationUser user = await _userManager.GetUserAsync(User);
            UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found

            data = data.Where(x => x.DirectionId.Equals(userProfile.DirectionId) && x.DepartementId.Equals(userProfile.DepartementId) && x.DernierStatut.ToLower().Contains("ACCEPTER"));
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.NomDemandeur.ToLower().Contains(searchValue.ToLower()) || x.DirectionLibelle.ToLower().Contains(searchValue.ToLower()) || x.DepartementLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            data = data.OrderBy(x => x.DernierStatut);
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return Json(returnObj);
        }

        [HttpPost]
        public async Task<JsonResult> GetExpressionBesoinsListValiderRespCrb()
        {
            int totalRecord = 0;
            int filterRecord = 0;
            var draw = Request.Form["draw"].FirstOrDefault();
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            var searchValue = Request.Form["search[value]"].FirstOrDefault();
            int pageSize = Convert.ToInt32(Request.Form["length"].FirstOrDefault() ?? "0");
            int skip = Convert.ToInt32(Request.Form["start"].FirstOrDefault() ?? "0");
            var data = _context.Set<ExpressionBesoin>().AsQueryable();
            ApplicationUser user = await _userManager.GetUserAsync(User);
            UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
            //get total count of data in table
            totalRecord = data.Count();
            // search data when search value found

            data = data.Where(x => x.DirectionId.Equals(userProfile.DirectionId) && x.DepartementId.Equals(userProfile.DepartementId) && x.DernierStatut.ToLower().Contains("VALIDCRB"));
            if (!string.IsNullOrEmpty(searchValue))
            {
                data = data.Where(x => x.NomDemandeur.ToLower().Contains(searchValue.ToLower()) || x.DirectionLibelle.ToLower().Contains(searchValue.ToLower()) || x.DepartementLibelle.ToLower().Contains(searchValue.ToLower()));
            }
            data = data.OrderBy(x => x.DernierStatut);
            // get total count of records after search
            filterRecord = data.Count();
            //sort data
            //if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortColumnDirection)) data = data.OrderBy(sortColumn + " " + sortColumnDirection);
            //pagination
            var empList = data.Skip(skip).Take(pageSize).ToList();
            var returnObj = new
            {
                draw = draw,
                recordsTotal = totalRecord,
                recordsFiltered = filterRecord,
                data = empList
            };

            return Json(returnObj);
        }
        // GET: ExpressionBesoins/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.ExpressionBesoin == null)
            {
                return NotFound();
            }

            var expressionBesoin = await _context.ExpressionBesoin
                .Include(e => e.UserProfile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (expressionBesoin == null)
            {
                return NotFound();
            }

            return View(expressionBesoin);
        }

        // GET: ExpressionBesoins/Create
        public IActionResult Create()
        {
            return PartialView("Create");
        }

        // POST: ExpressionBesoins/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ExpressionBesoin expressionBesoin)
        {
            if (ModelState.IsValid)
            {

                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));
                if (userProfile != null)
                {
                    expressionBesoin.UserProfileId = userProfile.UserProfileId;
                    expressionBesoin.UserProfile = userProfile;
                    expressionBesoin.UserIdResponsable = userProfile.UserProfileId;
                    expressionBesoin.DirectionId = userProfile.DirectionId;
                    expressionBesoin.DepartementId = userProfile.DepartementId;
                    expressionBesoin.FonctionId = userProfile.FonctiontId;
                    expressionBesoin.NomDemandeur = userProfile.FirstName + " " + userProfile.LastName;
                    expressionBesoin.DirectionLibelle = userProfile.LibelleDirection;
                    expressionBesoin.DepartementLibelle = userProfile.LibelleDepartement;
                    expressionBesoin.Fonction = userProfile.LibelleFonction;
                }
                expressionBesoin.NumExpressionBesoin = _numberSequence.GetNumberSequence("/DAF/DRHMG/SMG/");
                expressionBesoin.DateDemande = DateTime.Now;
                expressionBesoin.DernierStatut = StatutConts.CREER;
                expressionBesoin.DateStatut = DateTime.Now;
                _context.Add(expressionBesoin);
                await _context.SaveChangesAsync();
                return Content(expressionBesoin.Id.ToString());
            }
            return Content("NOK");
        }
        // Afficher formulaire Modifier une expression de besoin
        public async Task<IActionResult> ModifierExpressionBesoin(int? id)
        {
            if (id == null)
                return NotFound();

            var expression = await _context.ExpressionBesoin.FindAsync(id);
            if (expression == null)
                return NotFound();
            var ListDepartement = _context.Departement.Select(d => new { IdDepartement = d.Id, LibelleDepartement = d.Libelle + " | " + d.DirectionLibelle });
            ViewData["CrbEnCharge"] = new SelectList(ListDepartement, "IdDepartement", "LibelleDepartement");
            var ListSourceBudgetaire = _context.CentreBudgetaire.Select(c => new { IdCentreBudgetaire = c.Id, LibelleCB = c.LibelleCb });
            ViewData["CentreBudgetaire"] = new SelectList(ListSourceBudgetaire, "IdCentreBudgetaire", "LibelleCB");
            return PartialView("Modifier", expression);
        }
        //  Modifier une expression de besoin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ModifierExpressionBesoin(int id, string Commentaire, ExpressionBesoin expressionBesoin)
        {
            try
            {
                var expression = await _context.ExpressionBesoin.FindAsync(id);
                if (expressionBesoin == null)
                    return NotFound();

                ApplicationUser user = await _userManager.GetUserAsync(User);
                UserProfile userProfile = _context.UserProfile.SingleOrDefault(x => x.ApplicationUserId.Equals(user.Id));

                expression.Commentaire = expressionBesoin.Commentaire;
                expression.Observation = expressionBesoin.Observation;
                expression.NatureBesoin = expressionBesoin.NatureBesoin;
                expression.MotifDemande = expressionBesoin.MotifDemande;
                //expression.DernierStatut = StatutConts.VALIDER;
                expression.CrbEnCharge = expressionBesoin.CrbEnCharge;
                expression.SourceBudgetaire = expressionBesoin.SourceBudgetaire;
                expression.DateStatut = DateTime.Now;

                _context.Update(expression);
                await _context.SaveChangesAsync();
                return Content(expression.DernierStatut);
            }
            catch (Exception)
            {
                //throw;
            }
            return Content("NOK");
        }


        // GET: ExpressionBesoins/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.ExpressionBesoin == null)
            {
                return NotFound();
            }

            var expressionBesoin = await _context.ExpressionBesoin.FindAsync(id);
            if (expressionBesoin == null)
            {
                return NotFound();
            }
            ViewData["UserProfileId"] = new SelectList(_context.UserProfile, "UserProfileId", "UserProfileId", expressionBesoin.UserProfileId);
            return View(expressionBesoin);
        }

        // POST: ExpressionBesoins/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserProfileId,UserIdResponsable,DirectionId,DepartementId,FonctionId,NomDemandeur,DirectionLibelle,DepartementLibelle,Fonction,NumExpressionBesoin,NatureBesoin,MotifDemande,DateDemande,DernierStatut,DateStatut,Observation,Commentaire")] ExpressionBesoin expressionBesoin)
        {
            if (id != expressionBesoin.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(expressionBesoin);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ExpressionBesoinExists(expressionBesoin.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["UserProfileId"] = new SelectList(_context.UserProfile, "UserProfileId", "UserProfileId", expressionBesoin.UserProfileId);
            return View(expressionBesoin);
        }

        // GET: ExpressionBesoins/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.ExpressionBesoin == null)
            {
                return NotFound();
            }

            var expressionBesoin = await _context.ExpressionBesoin
                .Include(e => e.UserProfile)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (expressionBesoin == null)
            {
                return NotFound();
            }

            return View(expressionBesoin);
        }

        // POST: ExpressionBesoins/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.ExpressionBesoin == null)
            {
                return Problem("Entity set 'ApplicationDbContext.ExpressionBesoin'  is null.");
            }
            var expressionBesoin = await _context.ExpressionBesoin.FindAsync(id);
            if (expressionBesoin != null)
            {
                _context.ExpressionBesoin.Remove(expressionBesoin);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ExpressionBesoinExists(int id)
        {
          return _context.ExpressionBesoin.Any(e => e.Id == id);
        }

        
    }
}
