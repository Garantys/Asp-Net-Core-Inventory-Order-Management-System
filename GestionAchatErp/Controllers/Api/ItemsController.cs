﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Items")]
    public class ItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Items
        [HttpGet]
        public async Task<IActionResult> GetItems()
        {
            List<Items> Items = await _context.Items.ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<Items> payload)
        {
            Items Items = payload.value;
            _context.Items.Add(Items);
            _context.SaveChanges();
            return Ok(Items);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<Items> payload)
        {
            Items Items = payload.value;
            _context.Items.Update(Items);
            _context.SaveChanges();
            return Ok(Items);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<Items> payload)
        {
            Items Items = _context.Items
                .Where(x => x.Id == (int)payload.key)
                .FirstOrDefault();
            _context.Items.Remove(Items);
            _context.SaveChanges();
            return Ok(Items);

        }


    }
}
