﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/TypeMarche")]
    public class TypeMarcheController : Controller
    {
        private readonly ApplicationDbContext _context;

        public TypeMarcheController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/TypeMarche
        [HttpGet]
        public async Task<IActionResult> GetTypeMarche()
        {
            List<TypeMarche> TypeMarche = await _context.TypeMarche.ToListAsync();
            int Count = TypeMarche.Count();
            var jsonData = new { recordsFiltered = Count, recordsTotal = Count, data = TypeMarche, Items = TypeMarche, Count = Count };
            return Ok(jsonData);
        }

        [HttpPost("[action]")]
        public IActionResult GetTypeMarcheList()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnTypeMarche = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var customerData = (from d in _context.TypeMarche
                                    select d);
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnTypeMarche)))
                {
                    if (sortColumnTypeMarche.ToLower() == "desc")
                        customerData = customerData.OrderByDescending(d => sortColumn);
                    else
                        customerData = customerData.OrderBy(d => sortColumn);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.Code.Contains(searchValue)
                                                || m.Libelle.Contains(searchValue));
                }
                recordsTotal = customerData.Count();
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<TypeMarche> payload)
        {
            TypeMarche TypeMarche = payload.value;
            _context.TypeMarche.Add(TypeMarche);
            _context.SaveChanges();
            return Ok(TypeMarche);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<TypeMarche> payload)
        {
            TypeMarche TypeMarche = payload.value;
            _context.TypeMarche.Update(TypeMarche);
            _context.SaveChanges();
            return Ok(TypeMarche);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<TypeMarche> payload)
        {
            TypeMarche TypeMarche = _context.TypeMarche
                .Where(x => x.Id == (int)payload.key)
                .FirstOrDefault();
            _context.TypeMarche.Remove(TypeMarche);
            _context.SaveChanges();
            return Ok(TypeMarche);

        }


    }
}
