﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/DemandeAchatNatures")]
    public class DemandeAchatNaturesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DemandeAchatNaturesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/DemandeAchatNature
        [HttpGet]
        public async Task<IActionResult> GetDemandeAchatNatures()
        {
            List<DemandeAchatNature> Items = await _context.DemandeAchatNatures.ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<DemandeAchatNature> payload)
        {
            DemandeAchatNature demandeAchatNature = payload.value;
            _context.DemandeAchatNatures.Add(demandeAchatNature);
            _context.SaveChanges();
            return Ok(demandeAchatNature);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<DemandeAchatNature> payload)
        {
            DemandeAchatNature demandeAchatNature = payload.value;
            _context.DemandeAchatNatures.Update(demandeAchatNature);
            _context.SaveChanges();
            return Ok(demandeAchatNature);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<DemandeAchatNature> payload)
        {
            DemandeAchatNature demandeAchatNature = _context.DemandeAchatNatures
                .Where(x => x.DemandeAchatNatureId == (int)payload.key)
                .FirstOrDefault();
            _context.DemandeAchatNatures.Remove(demandeAchatNature);
            _context.SaveChanges();
            return Ok(demandeAchatNature);

        }


    }
}
