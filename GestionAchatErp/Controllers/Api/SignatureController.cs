﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Signature")]
    public class SignaturesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SignaturesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Signature
        [HttpGet]
        public async Task<IActionResult> GetSignatures()
        {
            List<Signature> Signature = await _context.Signature.ToListAsync();
            int Count = Signature.Count();
            return Ok(new { Signature, Count });
        }


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<Signature> payload)
        {
            Signature Signature = payload.value;
            _context.Signature.Add(Signature);
            _context.SaveChanges();
            return Ok(Signature);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<Signature> payload)
        {
            Signature Signature = payload.value;
            _context.Signature.Update(Signature);
            _context.SaveChanges();
            return Ok(Signature);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<Signature> payload)
        {
            Signature Signature = _context.Signature
                 .Where(x => x.Id == (int)payload.key)
                 .FirstOrDefault();
            _context.Signature.Remove(Signature);
            _context.SaveChanges();
            return Ok(Signature);

        }


    }
}
