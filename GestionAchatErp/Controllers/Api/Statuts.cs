﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Statuts")]
    public class StatutsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StatutsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Statuts
        [HttpGet]
        public async Task<IActionResult> GetStatuts()
        {
            List<Statuts> Statuts = await _context.Statuts.ToListAsync();
            int Count = Statuts.Count();
            var jsonData = new { recordsFiltered = Count, recordsTotal = Count, data = Statuts, Items = Statuts, Count = Count };
            return Ok(jsonData);
        }

        [HttpPost("[action]")]
        public IActionResult GetStatutsList()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnStatut = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var customerData = (from d in _context.Statuts
                                    select d);
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnStatut)))
                {
                    if (sortColumnStatut.ToLower() == "desc")
                        customerData = customerData.OrderByDescending(d => sortColumn);
                    else
                        customerData = customerData.OrderBy(d => sortColumn);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.Etape.Contains(searchValue)
                                                || m.Libelle.Contains(searchValue));
                }
                recordsTotal = customerData.Count();
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<Statuts> payload)
        {
            Statuts Statuts = payload.value;
            _context.Statuts.Add(Statuts);
            _context.SaveChanges();
            return Ok(Statuts);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<Statuts> payload)
        {
            Statuts Statuts = payload.value;
            _context.Statuts.Update(Statuts);
            _context.SaveChanges();
            return Ok(Statuts);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<Statuts> payload)
        {
            Statuts Statuts = _context.Statuts
                .Where(x => x.Id == (int)payload.key)
                .FirstOrDefault();
            _context.Statuts.Remove(Statuts);
            _context.SaveChanges();
            return Ok(Statuts);

        }


    }
}
