﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/CategorieItems")]
    public class CategorieItemsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CategorieItemsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/CategorieItems
        [HttpGet]
        public async Task<IActionResult> GetCategorieItems()
        {
            List<CategorieItems> Items = await _context.CategorieItems.ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<CategorieItems> payload)
        {
            CategorieItems CategorieItems = payload.value;
            _context.CategorieItems.Add(CategorieItems);
            _context.SaveChanges();
            return Ok(CategorieItems);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<CategorieItems> payload)
        {
            CategorieItems CategorieItems = payload.value;
            _context.CategorieItems.Update(CategorieItems);
            _context.SaveChanges();
            return Ok(CategorieItems);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<CategorieItems> payload)
        {
            CategorieItems CategorieItems = _context.CategorieItems
                .Where(x => x.Id == (int)payload.key)
                .FirstOrDefault();
            _context.CategorieItems.Remove(CategorieItems);
            _context.SaveChanges();
            return Ok(CategorieItems);

        }


    }
}
