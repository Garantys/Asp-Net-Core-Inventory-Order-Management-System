﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/CentreBudgetaire")]
    public class CentreBudgetaireController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CentreBudgetaireController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/CentreBudgetaire
        [HttpGet]
        public async Task<IActionResult> GetCentreBudgetaire()
        {
            List<CentreBudgetaire> Items = await _context.CentreBudgetaire.ToListAsync();
            int Count = Items.Count();
            var jsonData = new { recordsFiltered = Count, recordsTotal = Count, data = Items, Items = Items, Count = Count };
            return Ok(jsonData);
        }

        [HttpPost("[action]")]
        public IActionResult GetCentreBudgetaireList()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnCentreBudgetaire = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var customerData = (from d in _context.CentreBudgetaire
                                    select d);
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnCentreBudgetaire)))
                {
                    if (sortColumnCentreBudgetaire.ToLower() == "desc")
                        customerData = customerData.OrderByDescending(d => sortColumn);
                    else
                        customerData = customerData.OrderBy(d => sortColumn);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.Code.Contains(searchValue)
                                                || m.LibelleCb.Contains(searchValue));
                }
                recordsTotal = customerData.Count();
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<CentreBudgetaire> payload)
        {
            CentreBudgetaire CentreBudgetaire = payload.value;
            _context.CentreBudgetaire.Add(CentreBudgetaire);
            _context.SaveChanges();
            return Ok(CentreBudgetaire);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<CentreBudgetaire> payload)
        {
            CentreBudgetaire CentreBudgetaire = payload.value;
            _context.CentreBudgetaire.Update(CentreBudgetaire);
            _context.SaveChanges();
            return Ok(CentreBudgetaire);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<CentreBudgetaire> payload)
        {
            CentreBudgetaire CentreBudgetaire = _context.CentreBudgetaire
                .Where(x => x.Id == (int)payload.key)
                .FirstOrDefault();
            _context.CentreBudgetaire.Remove(CentreBudgetaire);
            _context.SaveChanges();
            return Ok(CentreBudgetaire);

        }


    }
}
