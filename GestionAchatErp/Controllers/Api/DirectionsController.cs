﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;
using Newtonsoft.Json;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Directions")]
    public class DirectionsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DirectionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Directions
        [HttpGet]
        public async Task<IActionResult> GetDirections()
        {
            List<Direction> Items = await _context.Direction.ToListAsync();
            int Count = Items.Count();
            var jsonData = new { recordsFiltered = Count, recordsTotal = Count, data = Items, Items= Items, Count=Count };
            return Ok(jsonData);
        }

        [HttpPost("[action]")]
        public IActionResult GetDirectionList()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var customerData = (from d in _context.Direction
                                    select d);
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    if(sortColumnDirection.ToLower() == "desc")
                        customerData = customerData.OrderByDescending(d => sortColumn);
                    else
                        customerData = customerData.OrderBy(d => sortColumn);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    customerData = customerData.Where(m => m.Code.Contains(searchValue)
                                                || m.Libelle.Contains(searchValue));
                }
                recordsTotal = customerData.Count();
                var data = customerData.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<Direction> payload)
        {
            Direction Direction = payload.value;
            _context.Direction.Add(Direction);
            _context.SaveChanges();
            return Ok(Direction);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<Direction> payload)
        {
            Direction Direction = payload.value;
            _context.Direction.Update(Direction);
            _context.SaveChanges();
            return Ok(Direction);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<Direction> payload)
        {
            var id = payload.key.ToString();
            Direction Direction = _context.Direction
                .Where(x => x.Id == int.Parse(id))
                .FirstOrDefault();
            _context.Direction.Remove(Direction);
            _context.SaveChanges();
            return Ok(Direction);

        }


    }
}
