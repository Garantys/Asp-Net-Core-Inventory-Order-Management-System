﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GestionAchatErp.Data;
using GestionAchatErp.Models;
using GestionAchatErp.Models.SyncfusionViewModels;
using Microsoft.DotNet.MSIdentity.Shared;
using Newtonsoft.Json;

namespace GestionAchatErp.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/Departements")]
    public class DepartementsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DepartementsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Departements
        [HttpGet]
        public async Task<IActionResult> GetDepartements()
        {
            List<Departement> Items = await _context.Departement.ToListAsync();
            int Count = Items.Count();
            return Ok(new { Items, Count });
        }

        [HttpGet("[action]/{id}")]
        public  JsonResult GetByDirectionId([FromRoute] int id)
        {
            List<Departement> departement = null;
            Direction direction  =  _context.Direction.FirstOrDefault(x => x.Id == id);
            if (direction != null)
            {
                 departement = _context.Departement.Where(x => x.DirectionId == direction.Id).ToList();
            }
            var data = JsonConvert.SerializeObject(departement);

            return Json(data);
        }

        [HttpPost("[action]")]
        public IActionResult Insert([FromBody] CrudViewModel<Departement> payload)
        {
            Departement Departement = payload.value;
            _context.Departement.Add(Departement);
            _context.SaveChanges();
            return Ok(Departement);
        }

        [HttpPost("[action]")]
        public IActionResult Update([FromBody] CrudViewModel<Departement> payload)
        {
            Departement Departement = payload.value;
            _context.Departement.Update(Departement);
            _context.SaveChanges();
            return Ok(Departement);
        }

        [HttpPost("[action]")]
        public IActionResult Remove([FromBody] CrudViewModel<Departement> payload)
        {
            Departement Departement = _context.Departement
                .Where(x => x.Id == (int)payload.key)
                .FirstOrDefault();
            _context.Departement.Remove(Departement);
            _context.SaveChanges();
            return Ok(Departement);

        }


    }
}
